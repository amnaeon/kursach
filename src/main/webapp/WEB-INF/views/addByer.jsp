<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 09.05.17
  Time: 12:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>add byer</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 10% 28% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }

        br {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<div class="div">
    <spring:form method="post" modelAttribute="byerModel" action="addNewByer">
        <h4>Выбирете тип телефона</h4>
        <p>
            <form:select path="telephoneTypeName">
                <spring:options items="${telephoneData}"/>
            </form:select>
        </p>
        <hr>
        <p> <spring:input path="email" placeholder = "Введите email"/></p>
        <p> <spring:input path="telephone" placeholder = "Введите телефон"/></p>
        <p> <spring:input path="name" placeholder = "Введите имя"/></p>
        <p> <spring:input path="sername" placeholder ="Введите фамилию "/></p>
        <p><spring:input path="patronimic" placeholder="Введите отчество "/></p>
        <input type="submit" value="добавить">
    </spring:form>
</div>
</body>
</html>
