<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Sales Contracts</title>
</head>
<body>
<div align="center">
    <h1>Sales Contract List</h1>
    <table border="1">
        <th>Sales ID</th>
        <th>Date</th>
        <th>Byer ID</th>

        <c:forEach var="sales" items="${salesContractsModelList}" varStatus="status">
            <tr>
                <td>${sales.salesId}</td>
                <td>${sales.date}</td>
                <td>${sales.byerId}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>