<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Addresses</title>
</head>
<body>
<div align="center">
    <h1>Address List</h1>
    <table border="1">
        <th>Address ID</th>
        <th>Postcode</th>
        <th>Street ID</th>

        <c:forEach var="address" items="${addressModelList}" varStatus="status">
            <tr>
                <td>${address.addressId}</td>
                <td>${address.postcode}</td>
                <td>${address.streetId}</td>

            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>