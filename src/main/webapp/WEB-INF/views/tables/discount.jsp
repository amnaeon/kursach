<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Discounts</title>
</head>
<body>
<div align="center">
    <h1>Discount List</h1>
    <table border="1">
        <th>Discount ID</th>
        <th>Discount Name </th>
        <th>Discount Persent</th>

        <c:forEach var="discount" items="${dicountModelList}" varStatus="status">
            <tr>
                <td>${discount.discountId}</td>
                <td>${discount.discountName}</td>
                <td>${discount.discountPersent}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>