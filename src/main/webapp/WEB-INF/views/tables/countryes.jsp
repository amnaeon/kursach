<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Countryes</title>
</head>
<body>
<div align="center">
    <h1>Country List</h1>
    <table border="1">
        <th>Country ID</th>
        <th>Name</th>
        <th>Telephone code</th>

        <c:forEach var="country" items="${countryModelList}" varStatus="status">
            <tr>
                <td>${country.countryId}</td>
                <td>${country.name}</td>
                <td>${country.telephoneCode}</td>

            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>