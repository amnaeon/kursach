<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Streets</title>
</head>
<body>
<div align="center">
    <h1>Streets List</h1>
    <table border="1">
        <th>Street ID</th>
        <th>Name</th>
        <th>Street Type ID</th>
        <th>Locaity Type ID</th>

        <c:forEach var="street" items="${streetModelList}" varStatus="status">
            <tr>
                <td>${street.streetId}</td>
                <td>${street.name}</td>
                <td>${street.streetTypeId}</td>
                <td>${street.locaityTypeId}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>