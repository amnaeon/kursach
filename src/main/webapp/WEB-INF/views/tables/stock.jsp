<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Stocks</title>
</head>
<body>
<div align="center">
    <h1>Stock List</h1>
    <table border="1">
        <th>Availability Code</th>
        <th>Availability Status</th>

        <c:forEach var="stock" items="${stockModelList}" varStatus="status">
            <tr>
                <td>${stock.availabilityCode}</td>
                <td>${stock.availabilityStatus}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>