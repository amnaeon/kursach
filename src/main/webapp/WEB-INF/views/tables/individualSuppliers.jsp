<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Individual Suppliers</title>
</head>
<body>
<div align="center">
    <h1>Individual Suppliers List</h1>
    <table border="1">
        <th>Supplier ID</th>
        <th>Document ID</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Patronymic</th>


        <c:forEach var="individualSuppliers" items="${individualSuppliersModelList}" varStatus="status">
            <tr>
                <td>${individualSuppliers.supplierId}</td>
                <td>${individualSuppliers.documentId}</td>
                <td>${individualSuppliers.name}</td>
                <td>${individualSuppliers.surname}</td>
                <td>${individualSuppliers.patronymic}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>