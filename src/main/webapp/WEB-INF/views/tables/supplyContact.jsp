<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Supply Contracts</title>
</head>
<body>
<div align="center">
    <h1>Supply Contract List</h1>
    <table border="1">
        <th>Supply Contract ID</th>
        <th>Couclusion Date</th>
        <th>Expiration Date</th>
        <th>Delivery Date</th>

        <c:forEach var="supplyContract" items="${supplyContractModelList}" varStatus="status">
            <tr>
                <td>${supplyContract.supplyContractId}</td>
                <td>${supplyContract.couclusionDate}</td>
                <td>${supplyContract.expirationDate}</td>
                <td>${supplyContract.deliveryDate}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>