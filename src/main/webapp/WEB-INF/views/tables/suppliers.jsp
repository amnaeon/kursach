<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Suppliers</title>
</head>
<body>
<div align="center">
    <h1>Suppliers List</h1>
    <table border="1">
        <th>Suppliers ID</th>
        <th>Address ID</th>
        <th>Telephone Type ID</th>
        <th>Phone</th>

        <c:forEach var="supplier" items="${supplierModelList}" varStatus="status">
            <tr>
                <td>${supplier.suppliersId}</td>
                <td>${supplier.addressId}</td>
                <td>${supplier.telephoneTypeId}</td>
                <td>${supplier.phone}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>