<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Byers</title>
</head>
<body>
<div align="center">
    <h1>Byer List</h1>
    <table border="1">
        <th>Byer Id</th>
        <th>Telephone type ID</th>
        <th>Email</th>

        <c:forEach var="byer" items="${byerModelList}" varStatus="status">
            <tr>
                <td>${byer.byerId}</td>
                <td>${byer.telephoneTypeId}</td>
                <td>${byer.email}</td>

            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>