<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 08.05.17
  Time: 13:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>sale form</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 10% 25%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 4px;
            border-radius: 10px;
            color: dimgray;
            margin-right: 15%;
            margin-left: 15%;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }
    </style>
</head>
<body>
<div class="div">
    <table border="1" style="margin-left: 5%">
        <th>Серийный номер</th>
        <th>Название</th>
        <th>Модель</th>
        <th>Дата выпуска</th>
        <th>Категория</th>
        <th>Производитель</th>
        <th>Цена</th>

        <c:forEach var="product" items="${productModelList}" varStatus="status">
            <tr>
                <td>${product.serialNumber}</td>
                <td>${product.name}</td>
                <td>${product.model}</td>
                <td>${product.releaseDate}</td>
                <td>${product.categoryName}</td>
                <td>${product.manufacturerName}</td>
                <td>${product.price}</td>

            </tr>
        </c:forEach>
    </table>
    <hr>
    <spring:form method="post" modelAttribute="saleData" action="saleProduct">
        <p><spring:input path="serialNumber" placeholder="Выберите серийный номер товара"/></p>
        <hr>
        <h2>Выберите покупателя или нажмите</h2>
        <p>
            <form:select path="byerName">
                <spring:options items="${data}"/>
            </form:select>
        </p>
        <div class="div1"><a href="${pageContext.request.contextPath}/addByer">добавить покупателя</a></div>
        <hr>
        <h4>Код договора продажи ${saleData.id}</h4>
        <hr>
        <p><input type="submit" value="продать"></p>
    </spring:form>
</div>
</body>
</html>
