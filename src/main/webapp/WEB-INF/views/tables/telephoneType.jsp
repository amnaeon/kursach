<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Telephones Type</title>
</head>
<body>
<div align="center">
    <h1>Telephone Type List</h1>
    <table border="1">
        <th>Telephone Type ID</th>
        <th>Name</th>


        <c:forEach var="telephoneType" items="${telephoneTypeModelList}" varStatus="status">
            <tr>
                <td>${telephoneType.telephoneTypeId}</td>
                <td>${telephoneType.name}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>