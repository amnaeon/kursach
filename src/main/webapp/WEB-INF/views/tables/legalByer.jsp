<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Legal Byers</title>
</head>
<body>
<div align="center">
    <h1>Legal Byer List</h1>
    <table border="1">
        <th>Byer ID</th>
        <th>Name</th>
        <th>Certificate Number</th>
        <th>Tax Number</th>


        <c:forEach var="legalByer" items="${legalByerModelList}" varStatus="status">
            <tr>
                <td>${legalByer.byerId}</td>
                <td>${legalByer.name}</td>
                <td>${legalByer.certificateNumber}</td>
                <td>${legalByer.taxNumber}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>