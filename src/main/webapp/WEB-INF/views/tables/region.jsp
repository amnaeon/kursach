<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Regions</title>
</head>
<body>
<div align="center">
    <h1>Region List</h1>
    <table border="1">
        <th>Region ID</th>
        <th>Name</th>
        <th>Country ID</th>

        <c:forEach var="region" items="${regionModelList}" varStatus="status">
            <tr>
                <td>${region.regionId}</td>
                <td>${region.name}</td>
                <td>${region.countryId}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>