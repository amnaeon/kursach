<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Localitys</title>
</head>
<body>
<div align="center">
    <h1>Locality List</h1>
    <table border="1">
        <th>Locality ID</th>
        <th>Telephone Code</th>
        <th>Name</th>
        <th>Region ID</th>
        <th>Locality Type ID</th>


        <c:forEach var="locality" items="${localityModelList}" varStatus="status">
            <tr>
                <td>${locality.localityId}</td>
                <td>${locality.telephoneCode}</td>
                <td>${locality.name}</td>
                <td>${locality.regionId}</td>
                <td>${locality.localityTypeId}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>