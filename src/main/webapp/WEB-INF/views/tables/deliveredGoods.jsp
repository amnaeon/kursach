<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Delivered Goods</title>
</head>
<body>
<div align="center">
    <h1>Delivered Goods List</h1>
    <table border="1">
        <th>Serial Number</th>
        <th>Supply Contract ID </th>
        <th>Count</th>

        <c:forEach var="goods" items="${deliveredGoodsModelList}" varStatus="status">
            <tr>
                <td>${goods.serialNumber}</td>
                <td>${goods.supplyContractId}</td>
                <td>${goods.count}</td>

            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>