<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Sold Goods</title>
</head>
<body>
<div align="center">
    <h1>Sold Goods List</h1>
    <table border="1">
        <th>Selling Goods ID</th>
        <th>Discount ID</th>
        <th>Count</th>
        <th>Currenccy ID</th>

        <c:forEach var="soldGoods" items="${soldGoodsModelList}" varStatus="status">
            <tr>
                <td>${soldGoods.sellingGoodsId}</td>
                <td>${soldGoods.discountId}</td>
                <td>${soldGoods.count}</td>
                <td>${soldGoods.currenccyId}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>