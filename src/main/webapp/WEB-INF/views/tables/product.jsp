<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Products</title>
</head>
<body>
<div align="center">
    <h1>Product List</h1>
    <table border="1">
        <th>SerialNumber</th>
        <th>Name</th>
        <th>Model</th>
        <th>ReleaseDate</th>
        <th>AvailabilityCode</th>
        <th>CategoryCode</th>
        <th>ManufacturerId</th>
        <th>Price</th>
        <th>Count</th>

        <c:forEach var="product" items="${productModelList}" varStatus="status">
            <tr>
                <td>${product.serialNumber}</td>
                <td>${product.name}</td>
                <td>${product.model}</td>
                <td>${product.releaseDate}</td>
                <td>${product.categoryCode}</td>
                <td>${product.avalibilityCode}</td>
                <td>${product.manufacturerId}</td>
                <td>${product.price}</td>
                <td>${product.сount}</td>

            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
