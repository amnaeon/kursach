<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Street Types</title>
</head>
<body>
<div align="center">
    <h1>Street Type List</h1>
    <table border="1">
        <th>Street Type ID</th>
        <th>Name</th>

        <c:forEach var="type" items="${streetTypeModelList}" varStatus="status">
            <tr>
                <td>${type.streetTypeId}</td>
                <td>${type.name}</td>

            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
