<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Goods Categorys</title>
</head>
<body>
<div align="center">
    <h1>Goods Category List</h1>
    <table border="1">
        <th>Category ID</th>
        <th>Category Name </th>

        <c:forEach var="goods" items="${goodsModelList}" varStatus="status">
            <tr>
                <td>${goods.categoryId}</td>
                <td>${goods.categoryName}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>