<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Legal Suppliers</title>
</head>
<body>
<div align="center">
    <h1>Legal Supplier List</h1>
    <table border="1">
        <th>Suppliers ID</th>
        <th>Name</th>
        <th>Certificate Number</th>
        <th>Tax Number</th>


        <c:forEach var="legalS" items="${legalSupplierModelList}" varStatus="status">
            <tr>
                <td>${legalS.suppliersId}</td>
                <td>${legalS.name}</td>
                <td>${legalS.certificateNumber}</td>
                <td>${legalS.taxNumber}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>