<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Locality Types</title>
</head>
<body>
<div align="center">
    <h1>Locality Type List</h1>
    <table border="1">
        <th>Locality Type ID</th>
        <th>Locality Name</th>

        <c:forEach var="localityType" items="${localityTypeModelList}" varStatus="status">
            <tr>
                <td>${localityType.localityTypeId}</td>
                <td>${localityType.localityName}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>