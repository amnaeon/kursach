<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>admin form</title>
    <style>
        p {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 10% 40% 5%;
            border-radius: 10px;
        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        a {
            text-decoration: none;
            color: dimgray;
        }
        .div1{
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
        }
    </style>
</head>
<body>
<div class="div">
    <jsp:include page="showAllTableForm.jsp"/>
    <div class="div1">
        <p><a href="showAddingWorker">Добавить работника</a></p>
    </div>
    <hr>
    <div class="div1">
        <p><a href="deleteWorker">Удалить работника</a></p>
    </div>
    <hr>
    <div class="div1">
        <p><a href="acceptanceOfGoods">Работа с товаром</a></p>
    </div>
    <hr>
</div>
</body>
</html>