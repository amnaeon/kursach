<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>seller form</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 4px;
            border-radius: 10px;
            color: dimgray;

        }
        a {
            text-decoration: none;
            color: dimgray;
        }
    </style>
</head>
<body>
<jsp:include page="showAllTableForm.jsp"/>
<div class="div">
    <div class="div1">
        <p><a href="sale">Продать товар</a></p>
        <hr>
    </div>
</div>
</body>
</html>