<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 30.05.17
  Time: 3:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>supply goods</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 10% 28% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }

        br {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script>
    $(function () {
        $("#conclusionDate").datepicker({
            maxDate:+0
        });
    });
</script>


<spring:form method="post" modelAttribute="productList" action="startSupply1">
    <div class="div">
        <table border="1">
            <th>Название</th>
            <th>Модель</th>
            <th>Категория товара</th>
            <th>Производитель</th>
            <th>Цена</th>
            <th>Дата Выпуска</th>
            <th>Серийный номер</th>

            <c:forEach var="goods" items="${productList.productModels}" varStatus="status">
                <tr>
                    <td>${goods.name}</td>
                    <td>${goods.model}</td>
                    <td>${goods.categoryName}</td>
                    <td>${goods.manufacturerName}</td>
                    <td>${goods.price}</td>
                    <td>${goods.releaseDate}</td>
                    <td>${goods.serialNumber}</td>
                </tr>

            </c:forEach>
        </table>
        <hr>
        <p><spring:input path="name" placeholder = "Введите название товара" cssStyle="margin-bottom: 10px"/></p>
        <p><spring:input path="date" id="conclusionDate" placeholder = "Выберите дату выпуска" value = "" cssStyle="margin-bottom: 10px"/></p>
        <p><spring:input path="serialNum" placeholder = "Введите серийный номер" cssStyle="margin-bottom: 10px"/></p>
        <hr>
        <p><input type="submit" value="Внесни"/></p>
    </div>
</spring:form>
</body>
</html>
