<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 08.05.17
  Time: 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>error</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }
    </style>
</head>
<body>
<div class="div">
    <spring:form method="post" modelAttribute="message">
        <h1>${message.message}</h1>
    </spring:form>
</div>
</body>
</html>
