<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 11.04.17
  Time: 6:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 4px;
            border-radius: 10px;
            color: dimgray;

        }
    </style>
</head>
<body>
<div class="div">
    <spring:form method="post" modelAttribute="user" action="check-user" cssClass="text">

        <spring:input path="login" cssStyle="margin-bottom: 15px" placeholder="Login"/>
        </br>
        <spring:password path="password" placeholder="Password"/>
        <hr>
        <input type="submit" value="Войти">

        </spring:form>
</div>
</body>
</html>
