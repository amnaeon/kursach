<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 02.05.17
  Time: 14:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>add new supply contract</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }

        br {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<spring:form method="post" modelAttribute="model" action="addNewSupplyContract">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script>
        $(function () {
            $("#conclusionDate").datepicker({
                maxDate:+0
            });
        });
        $(function () {
            $("#expirationDate").datepicker();
        });
        $(function () {
            $("#deliveryDate").datepicker();
        });
    </script>
    <div class="div">
        <p><spring:input path="couclusionDate" id="conclusionDate" placeholder="Дата заключение:" cssStyle="margin-bottom: 10px"/></p>
        <p><spring:input path="expirationDate" id="expirationDate" placeholder="Дата окончания:" cssStyle="margin-bottom: 10px"/></p>
        <p><spring:input path="deliveryDate" id="deliveryDate" placeholder="Дата поставки:"/></p>
        <p><input type="text" readonly="readonly" value="${model.supplyContractId}" placeholder="Номер договора"/></p>
        <hr>
        <spring:hidden path="supplyContractId" value="${model.supplyContractId}"/>
        <input type="submit" value="добавить"/>
    </div>
</spring:form>
</body>
</html>
