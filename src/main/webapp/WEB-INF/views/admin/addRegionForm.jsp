<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 23.04.17
  Time: 16:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>add region form</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }

        br {
            margin-bottom: 8px;
        }
    </style>
</head>
<body>
<div class="div">
    <spring:form method="post" modelAttribute="region" action="addNewRegion">
        <p><spring:input path="name" placeholder="Название Области:"/></p>
        <h2>Выберите страну</h2>
        <form:select path="countryName" cssStyle="margin-bottom: 10px">
            <spring:options items="${dataList}"/>
        </form:select>
        <hr>
        <p><spring:button>Добавить область</spring:button></p>
    </spring:form>
</div>
</body>
</html>
