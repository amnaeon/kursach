<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 04.05.17
  Time: 11:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>add product</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }

        br {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script>
    $(function () {
        $("#datepicker").datepicker({
            maxDate: +0
        });
    });

</script>
<spring:form method="post" modelAttribute="product" action="addProduct">
    <div class="div">
        <p><spring:input path="name" placeholder="Введите название "/></p>
        <p><spring:input path="model" placeholder="Введите модель"/></p>
        <p><spring:input path="price" placeholder="Введите цену"/></p>
        <p><spring:input path="сount" placeholder="Введите количество"/></p>
        <h2>Выберите категорию товара</h2>
        <p>
            <spring:select path="categoryName">
                <spring:options items="${categoryList}"/>
            </spring:select>
        </p>
        <hr>
        <h2>Выберите производителя или нажмите </h2>
        <div class="div1">
            <a href="${pageContext.request.contextPath}/addManufacturer">добавить
                производителя </a>
        </div>
        <p>
            <spring:select path="manufacturerName">
                <spring:options items="${manufacturerList}"/>
            </spring:select>
        </p>
        <hr>
        <input type="submit" value="Добавить"/>
    </div>
</spring:form>
</body>
</html>
