<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 02.05.17
  Time: 13:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Acceptance of goods selector</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }
    </style>
</head>
<body>
<div class="div">
    <h2>Выберите договор поставки или нажмите</h2>
    <div class="div1" style="margin-top: -5px">
        <a href="${pageContext.request.contextPath}/addNewSupplyContractBtn">Добавить
            новый договор</a>
    </div>
    <hr>
    <spring:form method="post" action="selectSupplyContract" modelAttribute="supplyContract">
        <spring:select path="supplyContractId">
            <spring:options items="${dataList}"/>
        </spring:select>
        <spring:hidden path="suppliersId" value="${sId}"/>
        <hr>
        <p><input type="submit" value="Далее"/></p>
    </spring:form>
</div>
</body>

</html>
