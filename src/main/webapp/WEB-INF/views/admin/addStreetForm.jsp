<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 23.04.17
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>add street form</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }

        br {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<div class="div">
    <spring:form method="post" modelAttribute="streetModel" action="addNewStreet">
        <p><spring:input path="name" placeholder="Название улицы"/></p>

        <h2>Выберите тип населенного пункта </h2>
        <form:select id="" path="localityTypeName" cssStyle="margin-bottom: 10px">
            <spring:options items="${localityTypeList}"/>
        </form:select>
        <hr>
        <h2>Выберите тип улицы </h2>
        <form:select id="" path="streetTypeName" cssStyle="margin-bottom: 10px">
            <spring:options items="${streetTypeList}"/>
        </form:select>
        <hr>
        <input type="submit" value="Добавить"/>

    </spring:form>
</div>
</body>
</html>
