<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 23.04.17
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>add locality form</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }

        br {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<div class="div">
    <spring:form method="post" modelAttribute="locality" action="addNewLocality">

        <spring:input path="telephoneCode" placeholder="Телефонный код населенного пункта" cssStyle="margin-bottom: 10px" />

        <spring:input path="name" placeholder="Название населенного пункта: " />
        <br>
        <h2>Выберите область или </h2>
        <spring:select id="" path="regionName" cssStyle="margin-bottom: 10px">
            <spring:options items="${regionList}"/>
        </spring:select>
        <br>
        <div class="div1" >
            <a href="${pageContext.request.contextPath}/addRegion">Добавьте новую область</a>
        </div>


        <hr>
        <h2>Выберите тип населенного пункта</h2>
        <spring:select id="" path="localityTypeName" cssStyle="margin-bottom: 10px">
            <spring:options items="${localityTypeList}"/>
        </spring:select>
        <hr>
        <p><spring:button>Добавить населенный пункт</spring:button></p>

    </spring:form>
</div>
</body>
</html>
