<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>add suppliers</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 5% 35% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 7px;
            margin-bottom: 7px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }
        br{
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<spring:form method="post" modelAttribute="suppliers" action="next">
<div class="div">
    <h2>Выберите страну со списка либо нажмите</h2>
    <form:select path="country" cssStyle="margin-bottom: 10px">
        <spring:options items="${countryList}"/>
    </form:select>
    <br>
    <div class="div1">
        <a href="${pageContext.request.contextPath}/addCountrys">Добавить
            страну</a>
    </div>
    <hr>
    <h2>Выберите область со списка либо нажмите</h2>
    <form:select path="region" cssStyle="margin-bottom: 10px">
        <spring:options items="${regionList}"/>
    </form:select>
    <br>
    <div class="div1">
        <a href="${pageContext.request.contextPath}/addRegion"> Добавить
            область</a>
    </div>
    <hr>
    <p>Выберите населенный пункт со списка либо нажмите</p>
    <form:select path="locality" cssStyle="margin-bottom: 10px">
        <spring:options items="${localityList}"/>
    </form:select>
    <br>

    <div class="div1">
        <a href="${pageContext.request.contextPath}/addLocality">добавить
            населенный пункт</a>
    </div>
    <hr>
    <h2>Выберите улицу со списка либо нажмите</h2>
    <form:select path="street" cssStyle="margin-bottom: 10px">
        <spring:options items="${streetList}"/>
    </form:select>
    <br>

    <div class="div1">
        <a href="${pageContext.request.contextPath}/addStreet">Добавить улицу</a>
    </div>
    <hr>
    <h2>Выбирете тип телефона</h2>
    <form:select path="telephoneCode" cssStyle="margin-bottom: 10px">
        <spring:options items="${telephoneTypeList}"/>
    </form:select>
    <hr>
    <p><spring:input path="buildNum" type="text" placeholder="введите номер дома"/></p>
    <hr>
    <p><spring:input path="telephoneNumber" type="text" placeholder="введите номер телефона(без кода страны)"/></p>
    <hr>
    <p><spring:input path="postCode" type="text" placeholder="введите почтовый индекс"/></p>
    <hr>
    <p>физическое лицо <spring:checkbox path="entity"/></p>
    <hr>
    <div>
        <input type="submit" value="далее">
    </div>
    </spring:form>
</div>
</body>
</html>
