<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Acceptance of goods</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }
    </style>
</head>
<body>
<div class="div">
    <h2>Выбирите поставщика или нажмите</h2>
    <div class="div1" style="margin-top: -5px">
        <a href="${pageContext.request.contextPath}/addSuppliers">Добавить поставщика</a>
    </div>
    <hr>
    <spring:form method="post" modelAttribute="data" action="acceptanceOfGood">
        <spring:select path="dName">
            <spring:options items="${dataList}"/>
        </spring:select>
        <hr>
        <p><input type="submit" value="далее"/></p>
    </spring:form>
</div>
</body>
</html>