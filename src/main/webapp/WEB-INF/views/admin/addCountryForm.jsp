<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>

<html>
<head>
    <title>add country form</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }
        br {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<div class="div">
    <spring:form method="post" modelAttribute="country" action="addNewCountry">
        <spring:input path="name"  placeholder="Название Страны:" cssStyle="margin-bottom: 10px"/>
        <br>
        <spring:input path="telephoneCode" placeholder="Код Телефона:"/>
        <hr>
        <spring:button>Добавить</spring:button>

    </spring:form>
</div>
</body>
</html>
