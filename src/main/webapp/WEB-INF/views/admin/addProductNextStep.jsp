<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>add product next step</title>
    <style>

        .div {
            text-align: center;

            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 10% 35% 5%;
            border-radius: 10px;
        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 4px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }
    </style>
</head>
<body>
<div class="div">
    <h1>Товар по договору</h1>
    <table border="1">
        <th>Имя</th>
        <th>Модель</th>
        <th>Катерогия</th>
        <th>Производитель</th>
        <th>Цена</th>
        <th>Количество</th>

        <c:forEach var="product" items="${product}" varStatus="status">
            <tr>
                <td>${product.name}</td>
                <td>${product.model}</td>
                <td>${product.categoryName}</td>
                <td>${product.manufacturerName}</td>
                <td>${product.price}</td>
                <td>${product.сount}</td>

            </tr>
        </c:forEach>
    </table>
    <div class="div1"><a href="${pageContext.request.contextPath}/addNewProduct">Добавить товар</a></div>
    <div class="div1"><a href="${pageContext.request.contextPath}/createContract">Создать договор</a></div>
</div>
</body>
</html>
