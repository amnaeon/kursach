<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 19.04.17
  Time: 12:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>add worker form</title>
    <title>Title</title>
    <style>

        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 4px;
            border-radius: 10px;
            color: dimgray;

        }
    </style>
</head>
<body>
<div class="div">
    <spring:form method="post" modelAttribute="user" action="add_user" cssClass="text">

        <spring:input path="login" cssStyle="margin-bottom: 15px" id="log" placeholder="Имя: "/>
        <br>
        <spring:password path="password" id="pa" placeholder="Пароль: "/>
        <hr>
            <spring:button>Добавить работника</spring:button>
    </spring:form>
</div>
</body>
</html>
