<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 04.05.17
  Time: 18:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>goods selector</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 10% 27% 5%;
            border-radius: 10px;
            text-align: center;

        }


        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }

        br {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<spring:form method="post" modelAttribute="productList" action="startSupply">
    <div class="div">
        <table border="1">
            <th>Название</th>
            <th>Модель</th>
            <th>Категория товара</th>
            <th>Производитель</th>
            <th>Количество</th>
            <th>Цена</th>

            <c:forEach var="goods" items="${productList.addProductModelList}" varStatus="status">
                <tr>
                    <td>${goods.name}</td>
                    <td>${goods.model}</td>
                    <td>${goods.categoryName}</td>
                    <td>${goods.manufacturerName}</td>
                    <td>${goods.сount}</td>
                    <td>${goods.price}</td>
                    <td><spring:checkbox path="selected"/></td>
                </tr>

            </c:forEach>
        </table>
        <hr>
        <h2>Отметьте галочками товар если он пришел правильно,</h2>
        <h2>если товар пришел не правильно нажмите</h2>
        <div class="div1">
            <a href="${pageContext.request.contextPath}/deleteContract"><h2>Отменить поставку</h2></a>
        </div>
        <h2>При отмене поставки договор будет расторжен</h2>
        <h2>Если весь товар пришел правильно и без повреждений то нажмите кнопку "Начать оприходование"</h2>
        <hr>
        <p><input type="submit" value="Начать оприходование"/></p>
    </div>
</spring:form>
</body>
</html>
