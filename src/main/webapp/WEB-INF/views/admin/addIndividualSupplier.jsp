<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 25.04.17
  Time: 11:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>add individual supplier</title>
    <style>
        .text {
            text-align: center;

        }

        .div {
            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 15% 40% 5%;
            border-radius: 10px;
            text-align: center;

        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 10px;
            border-radius: 10px;
            color: dimgray;

        }

        a {
            text-decoration: none;
            color: dimgray;
        }

        br {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<spring:form method="post" action="addNewSupplier" modelAttribute="supplier">
    <spring:hidden path="country" value="${supplier.country}"/>
    <spring:hidden path="region" value="${supplier.region}"/>
    <spring:hidden path="locality" value="${supplier.locality}"/>
    <spring:hidden path="street" value="${supplier.street}"/>
    <spring:hidden path="telephoneCode" value="${supplier.telephoneCode}"/>
    <spring:hidden path="telephoneNumber" value="${supplier.telephoneNumber}"/>
    <spring:hidden path="postCode" value="${supplier.postCode}"/>
    <spring:hidden path="entity" value="${supplier.entity}"/>
    <spring:hidden path="buildNum" value="${suppliers.buildNum}"/>
    <div class="div">
        <p><spring:input path="individualSuppliersModel.documentId" placeholder = "Номер документа:"/></p>
        <p><spring:input path="individualSuppliersModel.surname" placeholder = "Фамилия:"/></p>
        <p> <spring:input path="individualSuppliersModel.name" placeholder = "Имя:"/></p>
        <p> <spring:input path="individualSuppliersModel.patronymic" placeholder = "Отчество:"/></p>
        <hr>
        <p><input type="submit" value="Добавить"/></p>
    </div>
</spring:form>
</body>
</html>
