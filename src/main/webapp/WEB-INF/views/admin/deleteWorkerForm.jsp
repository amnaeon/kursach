<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: stus
  Date: 19.04.17
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>delete worker</title>
    <style>

        .div {
            text-align: center;

            align-items: center;
            background: lightgray;
            padding: 20px;
            margin: 10% 40% 5%;
            border-radius: 10px;
        }

        hr {
            margin-top: 5px;
            width: 80%;
        }

        .div1 {
            align-items: center;
            background: #bac2d1;
            padding: 4px;
            border-radius: 10px;
            color: dimgray;

        }
    </style>
</head>
<body>
<div class="div">
    <h1>Список работников</h1>
    <table border="1" style="align-items: center">
        <th>worker login</th>

        <c:forEach var="user" items="${userList}" varStatus="status">
            <tr>
                <td>${user.login}</td>

            </tr>
        </c:forEach>
    </table>
    <br>
    <hr>
    <h2>Введите login работника которого вы желаете удалить</h2>
    <br>
    <hr>
    <spring:form method="post" modelAttribute="user" action="deleteWorkerForm">
        <spring:input path="login" title="" cssStyle="margin-bottom: 15px" placeholder="Login"/>
        <hr>

            <spring:button>Удалить</spring:button>

    </spring:form>
</div>
</body>
</html>
