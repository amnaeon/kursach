package net.codejava.spring.managers;

import net.codejava.spring.model.ProductModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 30.05.17.
 */
public class ProductManager {
    private List<ProductModel> productModels = new ArrayList<>();
    private static ProductManager ourInstance = new ProductManager();

    public static ProductManager getInstance() {
        return ourInstance;
    }

    private ProductManager() {
    }

    public List<ProductModel> getProductModels() {
        return productModels;
    }

    public void setProductModels(List<ProductModel> productModels) {
        this.productModels = productModels;
    }


}
