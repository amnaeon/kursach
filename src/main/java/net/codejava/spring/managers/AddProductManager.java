package net.codejava.spring.managers;

import net.codejava.spring.model.AddProductModel;
import net.codejava.spring.model.SupplyContractModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 09.05.17.
 */
public class AddProductManager {
    private static AddProductManager ourInstance = new AddProductManager();
    private List<AddProductModel> addProductModelList = new ArrayList<>();
    private SupplyContractModel supplyContractModel = new SupplyContractModel();
    public static AddProductManager getInstance() {
        return ourInstance;
    }

    private AddProductManager() {
    }

    public List<AddProductModel> getAddProductModelList() {
        return addProductModelList;
    }

    public void setAddProductModelList(List<AddProductModel> addProductModelList) {
        this.addProductModelList = addProductModelList;
    }

    public SupplyContractModel getSupplyContractModel() {
        return supplyContractModel;
    }

    public void setSupplyContractModel(SupplyContractModel supplyContractModel) {
        this.supplyContractModel = supplyContractModel;
    }
}
