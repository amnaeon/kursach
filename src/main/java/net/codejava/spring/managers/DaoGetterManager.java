package net.codejava.spring.managers;

import net.codejava.spring.dao.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by stus on 26.04.17.
 */
public class DaoGetterManager {
    private static DaoGetterManager instance = null;

    private DaoGetterManager() {
    }

    public static synchronized DaoGetterManager getInstance() {
        if(instance == null)
            instance = new DaoGetterManager();
        return instance;
    }

    @Autowired
    private AddressDao addressDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private ByerDAO byerDAO;
    @Autowired
    private CountryesDao countryesDao;

    @Autowired
    private DeliveredGoodsDAO deliveredGoodsDAO;

    @Autowired
    private GoodsCategoryDAO goodsCategoryDAO;
    @Autowired
    private IndividualSuppliersDAO individualSuppliersDAO;
    @Autowired
    private LegalSuppliersDAO legalSuppliersDAO;
    @Autowired
    private LegalByerDAO legalByerDAO;
    @Autowired
    private LocalityDAO localityDAO;
    @Autowired
    private LocalityTypeDAO localityTypeDAO;
    @Autowired
    private ManufacturerDAO manufacturerDAO;
    @Autowired
    private RegionDAO regionDAO;
    @Autowired
    private SalesContractDAO salesContractDAO;
    @Autowired
    private SoldGoodsDAO soldGoodsDAO;
    @Autowired
    private StockDAO stockDAO;
    @Autowired
    private StreetsDAO streetsDAO;
    @Autowired
    private StreetsTypeDAO streetsTypeDAO;
    @Autowired
    private SuppliersDAO suppliersDAO;
    @Autowired
    private SupplyContractDAO supplyContractDAO;
    @Autowired
    private TelephoneTypeDAO telephoneTypeDAO;

    public AddressDao getAddressDao() {
        return addressDao;
    }

    public ProductDao getProductDao() {
        return productDao;
    }

    public ByerDAO getByerDAO() {
        return byerDAO;
    }

    public CountryesDao getCountryesDao() {
        return countryesDao;
    }


    public DeliveredGoodsDAO getDeliveredGoodsDAO() {
        return deliveredGoodsDAO;
    }

    public GoodsCategoryDAO getGoodsCategoryDAO() {
        return goodsCategoryDAO;
    }

    public IndividualSuppliersDAO getIndividualSuppliersDAO() {
        return individualSuppliersDAO;
    }

    public LegalSuppliersDAO getLegalSuppliersDAO() {
        return legalSuppliersDAO;
    }

    public LegalByerDAO getLegalByerDAO() {
        return legalByerDAO;
    }

    public LocalityDAO getLocalityDAO() {
        return localityDAO;
    }

    public LocalityTypeDAO getLocalityTypeDAO() {
        return localityTypeDAO;
    }

    public ManufacturerDAO getManufacturerDAO() {
        return manufacturerDAO;
    }

    public RegionDAO getRegionDAO() {
        return regionDAO;
    }

    public SalesContractDAO getSalesContractDAO() {
        return salesContractDAO;
    }

    public SoldGoodsDAO getSoldGoodsDAO() {
        return soldGoodsDAO;
    }

    public StockDAO getStockDAO() {
        return stockDAO;
    }

    public StreetsDAO getStreetsDAO() {
        return streetsDAO;
    }

    public StreetsTypeDAO getStreetsTypeDAO() {
        return streetsTypeDAO;
    }

    public SuppliersDAO getSuppliersDAO() {
        return suppliersDAO;
    }

    public SupplyContractDAO getSupplyContractDAO() {
        return supplyContractDAO;
    }

    public TelephoneTypeDAO getTelephoneTypeDAO() {
        return telephoneTypeDAO;
    }
}
