package net.codejava.spring.managers;

/**
 * Created by stus on 08.05.17.
 */
public class SallerManager {
    private long time = 0;
    private static SallerManager ourInstance = new SallerManager();

    public static SallerManager getInstance() {
        return ourInstance;
    }

    private SallerManager() {
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
