package net.codejava.spring.managers;

import net.codejava.spring.model.AddProductModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 04.05.17.
 */
public class SuppliresManager {
    private int suppliersId = 0;
    private List<AddProductModel> productModelList = new ArrayList<>();
    private static SuppliresManager ourInstance = new SuppliresManager();
    private String name = "";
    private String suppliersContract = "";

    public String getSuppliersContract() {
        return suppliersContract;
    }

    public void setSuppliersContract(String suppliersContract) {
        this.suppliersContract = suppliersContract;
    }

    public static SuppliresManager getInstance() {
        return ourInstance;
    }

    private SuppliresManager() {
    }

    public int getSuppliersId() {
        return suppliersId;
    }

    public void setSuppliersId(int suppliersId) {
        this.suppliersId = suppliersId;
    }

    public List<AddProductModel> getProductModelList() {
        return productModelList;
    }

    public void setProductModelList(List<AddProductModel> productModelList) {
        this.productModelList = productModelList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void removeBySerialNum(String name) {
        for(int i =0;i<productModelList.size();i++){
            if(name.trim().equalsIgnoreCase(productModelList.get(i).getSerialNumber())) {
                productModelList.remove(i);
                break;
            }
        }
    }
}
