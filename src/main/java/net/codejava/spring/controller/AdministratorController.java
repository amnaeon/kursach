package net.codejava.spring.controller;

import net.codejava.spring.Employee;
import net.codejava.spring.consts.Const;
import net.codejava.spring.dao.IndividualSuppliersDAO;
import net.codejava.spring.dao.LegalSuppliersDAO;
import net.codejava.spring.dao.SuppliersDAO;
import net.codejava.spring.dao.UserDAO;
import net.codejava.spring.dao.baseDAOs.UpdateTablesDAO;
import net.codejava.spring.managers.DaoGetterManager;
import net.codejava.spring.model.*;
import net.codejava.spring.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 19.04.17.
 */
@Controller
public class AdministratorController {
    @Autowired
    UserDAO userDAO;
    @Autowired
    SuppliersDAO suppliersDAO;
    @Autowired
    IndividualSuppliersDAO individualSuppliersDAO;
    @Autowired
    LegalSuppliersDAO legalSuppliersDAO;

    @RequestMapping(value = "showAddingWorker")
    public ModelAndView showAddingWorker(ModelAndView modelAndView) {
        modelAndView.addObject("user", new User());
        modelAndView.setViewName("admin/addWorkerForm");

        return modelAndView;
    }

    @RequestMapping(value = "/add_user")
    public ModelAndView add_user(@ModelAttribute("user") User user) {
        ModelAndView modelAndView = new ModelAndView();
        List<User> requestModelList = userDAO.userList(Const.User.GET_ALL_USER_TYPE);
        boolean isValid = true;
        for (User aRequestModelList : requestModelList) {
            if (aRequestModelList.getLogin().equals(user.getLogin().trim()))
                isValid = false;
        }
        if (user.getLogin().isEmpty() || user.getPassword().isEmpty()
                ||!StringUtils.isContainChar(user.getLogin(),user.getPassword())||
                user.getLogin().length()>20||user.getPassword().length()>20)
            isValid = false;
        if (isValid) {
            UpdateTablesDAO.saveOrUpdate(userDAO.getJdbcTemplate()
                    , new Object[]{null, "seller", user.getLogin().trim(), user.getPassword().trim()}
                    , "INSERT INTO `users`(`id`, `type`, `login`, `password`) VALUES (?,?,?,?)");
            modelAndView.setViewName("success");
            modelAndView.addObject("message", new CallbackModel("Добавление прошло успешно"));
        } else {
            modelAndView.setViewName("error");
            modelAndView.addObject("message", new CallbackModel("Неверно введены данные или такой работник уже существует"));

        }
        return modelAndView;
    }

    @RequestMapping(value = "/deleteWorker")
    public ModelAndView showAddress(ModelAndView model) {
        List<User> userList = userDAO.userList(Const.User.GET_WORKER_TYPE);
        model.addObject(userList);
        model.addObject("user", new User());
        model.setViewName("admin/deleteWorkerForm");
        return model;
    }

    @RequestMapping(value = "/deleteWorkerForm")
    public ModelAndView deleteWorkerForm(@ModelAttribute("user") User model) {
        UpdateTablesDAO.delete(userDAO.getJdbcTemplate(), new Object[]{model.getLogin()}
                , "DELETE FROM users WHERE login=? AND type = 'seller'");
        ModelAndView modelAndView = new ModelAndView();
        List<User> userList = userDAO.userList(Const.User.GET_WORKER_TYPE);
        modelAndView.addObject(userList);
        modelAndView.setViewName("admin/deleteWorkerForm");
        return modelAndView;
    }

    @RequestMapping(value = "/acceptanceOfGoods")
    public ModelAndView acceptanceOfGoods(ModelAndView model) {
        DropDownValuesModel data = new DropDownValuesModel();
        ModelAndView mv = new ModelAndView();
        mv.addObject("data", data);

        List<String> dataList = new ArrayList<>();
        dataList.addAll(SuppliersModel.getNameList(suppliersDAO.suppliersModelList(), individualSuppliersDAO.individualSuppliersModels(), legalSuppliersDAO.legalSuppliersModels()));

        mv.addObject("dataList", dataList);
        mv.setViewName("admin/acceptanceOfGoodsForm");
        return mv;
    }

    @RequestMapping(value = "/add")
    public ModelAndView add(ModelAndView model) {
        model.setViewName("admin/acceptanceOfGoodsForm");
        return model;
    }

}
