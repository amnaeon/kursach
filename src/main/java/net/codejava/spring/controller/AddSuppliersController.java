package net.codejava.spring.controller;

import net.codejava.spring.dao.*;
import net.codejava.spring.dao.baseDAOs.UpdateTablesDAO;
import net.codejava.spring.dao.storedProsedure.SupplierSP;
import net.codejava.spring.model.*;
import net.codejava.spring.utils.StringUtils;
import net.codejava.spring.validators.DropDownValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AddSuppliersController {
    @Autowired
    CountryesDao countryesDao;
    @Autowired
    SuppliersDAO suppliersDAO;
    @Autowired
    RegionDAO regionDAO;
    @Autowired
    LocalityTypeDAO localityTypeDAO;
    @Autowired
    StreetsTypeDAO streetsTypeDAO;
    @Autowired
    StreetsDAO streetsDAO;
    @Autowired
    LocalityDAO localityDAO;
    @Autowired
    TelephoneTypeDAO telephoneTypeDAO;
    @Autowired
    IndividualSuppliersDAO individualSuppliersDAO;
    @Autowired
    LegalSuppliersDAO legalSuppliersDAO;
    @Autowired
    AddressDao addressDao;

    @RequestMapping(value = "/addSuppliers")
    public ModelAndView addSuppliers(ModelAndView model) {
        List<String> countryList = getCountryList();
        List<String> regionList = getRegionList();
        List<String> localityList = getLocalityList();
        List<String> streetList = getStreetList();
        List<String> telephoneTypeList = getTelephoneType();

        model.addObject("countryList", countryList);
        model.addObject("regionList", regionList);
        model.addObject("localityList", localityList);
        model.addObject("streetList", streetList);
        model.addObject("telephoneTypeList", telephoneTypeList);

        model.addObject("suppliers", new NewSupplierInfoModel());
        model.setViewName("admin/addSuppliersForm");
        return model;
    }

    @RequestMapping(value = "/next")
    public ModelAndView next(@ModelAttribute("suppliers") NewSupplierInfoModel suppliers) {
        ModelAndView model = new ModelAndView();
        if (suppliers.getTelephoneNumber().isEmpty() || suppliers.getPostCode().isEmpty()
                || suppliers.getBuildNum().isEmpty()
                || !StringUtils.isContainChar(suppliers.getBuildNum())
                || !StringUtils.NumValidator(suppliers.getTelephoneNumber())
                || !StringUtils.NumValidator(suppliers.getPostCode())) {
            model.setViewName("error");
            model.addObject("message", new CallbackModel("Неверно введенные данные"));
        } else {
            if (checkAddress(suppliers)) {
                model.addObject("supplier", suppliers);
                if (!suppliers.isEntity()) {
                    model.setViewName("admin/addLegalSupplier");
                } else {
                    model.setViewName("admin/addIndividualSupplier");
                }
            } else {
                model.setViewName("error");
                model.addObject("message", new CallbackModel("По таким данным не возможно добавить адресс"));
            }
        }
        return model;
    }

    private boolean checkAddress(NewSupplierInfoModel suppliers) {
        List<CountryesModel> countryesModelList = countryesDao.addressList();
        List<RegionModel> regionModelList = regionDAO.regionModels();
        List<LocalityModel> localityModelList = localityDAO.localityModels();
        List<StreetsModel> streetsModelList = streetsDAO.streetsModelList();
        int countryId = 0;
        int regionId = 0;
        int localityId = 0;
        int streetId = 0;
        boolean isValid = false;
        for (int i = 0; i < countryesModelList.size(); i++) {
            if (suppliers.getCountry().equalsIgnoreCase(countryesModelList.get(i).getName())) {
                countryId = countryesModelList.get(i).getCountryId();
            }
        }

        for (int i = 0; i < regionModelList.size(); i++) {
            if (suppliers.getRegion().equalsIgnoreCase(regionModelList.get(i).getName())) {
                regionId = countryesModelList.get(i).getCountryId();
            }
        }

        for (int i = 0; i < regionModelList.size(); i++) {
            if (regionModelList.get(i).getRegionId() == regionId) {
                if (regionModelList.get(i).getCountryId() == countryId)
                    isValid = true;
            }
        }
        if (!isValid)
            return false;

        isValid = false;
        for (int i = 0; i < localityModelList.size(); i++) {
            if (suppliers.getLocality().equalsIgnoreCase(localityModelList.get(i).getName())) {
                localityId = localityModelList.get(i).getLocalityId();
            }
        }

        for (int i = 0; i < localityModelList.size(); i++) {
            if (localityModelList.get(i).getLocalityId() == localityId) {
                if (localityModelList.get(i).getRegionId() == regionId)
                    isValid = true;
            }
        }
        if (!isValid)
            return false;

        isValid = false;

        for (int i = 0; i < streetsModelList.size(); i++) {
            if (suppliers.getStreet().equalsIgnoreCase(streetsModelList.get(i).getName())) {
                streetId = streetsModelList.get(i).getStreetId();
            }
        }
        for (int i = 0; i < localityModelList.size(); i++) {
            if (streetsModelList.get(i).getStreetId() == streetId) {
                if (streetsModelList.get(i).getLocaityTypeId() == localityId)
                    isValid = true;
            }
        }
        if (!isValid)
            return false;

        return true;
    }

    @RequestMapping(value = "/addCountrys")
    public ModelAndView addCountry(ModelAndView model) {
        model.addObject("country", new CountryesModel());
        model.setViewName("admin/addCountryForm");
        return model;
    }

    @RequestMapping(value = "/addRegion")
    public ModelAndView addRegion(ModelAndView model) {
        DropDownValuesModel data = new DropDownValuesModel();
        ModelAndView mv = new ModelAndView();
        mv.addObject("data", data);

        List<String> dataList = new ArrayList<>();
        dataList.addAll(CountryesModel.getNameList(countryesDao.addressList()));

        mv.addObject("dataList", dataList);
        mv.addObject("region", new RegionModel());
        mv.setViewName("admin/addRegionForm");
        return mv;
    }

    @RequestMapping(value = "/addLocality")
    public ModelAndView addLocality(ModelAndView model) {
        List<String> regionList = getRegionList();
        List<String> localityTypeList = getLocalityTypeList();
        model.addObject("regionList", regionList);
        model.addObject("localityTypeList", localityTypeList);
        model.addObject("locality", new LocalityModel());
        model.setViewName("admin/addLocalityForm");
        return model;
    }

    @RequestMapping(value = "/addStreet")
    public ModelAndView addStreet(ModelAndView model) {
        StreetsModel streetsModel = new StreetsModel();
        ModelAndView mv = new ModelAndView();
        mv.addObject("streetModel", streetsModel);

        List<String> streetType = new ArrayList<>();
        List<String> localityType = new ArrayList<>();
        streetType.addAll(StreetTypeModel.getNameList(streetsTypeDAO.streetTypeList()));
        localityType.addAll(LocalityTypeModel.getNameList(localityTypeDAO.localityTypeModels()));
        mv.addObject("streetTypeList", streetType);
        mv.addObject("localityTypeList", localityType);
        mv.setViewName("admin/addStreetForm");
        return mv;


    }

    @RequestMapping(value = "addNewCountry")
    public ModelAndView addNewCountry(@ModelAttribute("country") CountryesModel model) {
        ModelAndView modelAndView = new ModelAndView();
        if (model.isValid()) {
            List<CountryesModel> requestModelList = countryesDao.addressList();
            boolean isValid = true;
            for (CountryesModel aRequestModelList : requestModelList) {
                if (aRequestModelList.getName().equals(model.getName().trim())
                        || aRequestModelList.getTelephoneCode().equals(model.getName().trim()))
                    isValid = false;
            }
            if (isValid) {
                UpdateTablesDAO.saveOrUpdate(countryesDao.getJdbcTemplate()
                        , new Object[]{null, model.getName().trim(), model.getTelephoneCode().trim()}
                        , "INSERT INTO `countries`(`countryId`, `name`, `telephoneCode`) VALUES (?,?,?)");
                modelAndView.setViewName("success");
                modelAndView.addObject("message", new CallbackModel("Добавление прошло успешно"));
            } else {
                modelAndView.setViewName("error");
                modelAndView.addObject("message", new CallbackModel("Такая страна уже существует"));

            }
        } else {
            modelAndView.setViewName("error");
            modelAndView.addObject("message", new CallbackModel("Неверно введены данные"));
        }
        return modelAndView;
    }

    @RequestMapping(value = "addNewRegion")
    public ModelAndView addNewRegion(Model model,
                                     @ModelAttribute("region") RegionModel region) {
        ModelAndView modelAndView = new ModelAndView();
        if (region.isValid()) {
            List<RegionModel> regionModelList = regionDAO.regionModels();
            List<CountryesModel> countryesModelList = countryesDao.addressList();

            boolean isValid = true;
            for (RegionModel aRequestModelList : regionModelList) {
                if (aRequestModelList.getName().equals(region.getName().trim()))
                    isValid = false;
            }
            int countryType = 0;
            for (CountryesModel countryesModel : countryesModelList) {
                if (countryesModel.getName().equalsIgnoreCase(region.getCountryName()))
                    countryType = countryesModel.getCountryId();
            }
            if (isValid) {
                UpdateTablesDAO.saveOrUpdate(regionDAO.getJdbcTemplate()
                        , new Object[]{null, region.getName().trim(), countryType}
                        , "INSERT INTO `region`(`regionId`, `name`, `countryId`) VALUES (?,?,?)");
                modelAndView.setViewName("success");
                modelAndView.addObject("message", new CallbackModel("Добавление прошло успешно"));
            } else {
                modelAndView.setViewName("error");
                modelAndView.addObject("message", new CallbackModel("Такая область уже существует"));
            }
        } else {
            modelAndView.setViewName("error");
            modelAndView.addObject("message", new CallbackModel("Неверно введены данные"));
        }
        return modelAndView;
    }

    @RequestMapping(value = "addNewStreet")
    public ModelAndView addNewStreet(Model model,
                                     @ModelAttribute("streetModel") StreetsModel streetModel) {
        ModelAndView modelAndView = new ModelAndView();
        if (streetModel.isValid()) {
            List<StreetTypeModel> streetTypeModelList = streetsTypeDAO.streetTypeList();
            List<LocalityTypeModel> localityTypeModelList = localityTypeDAO.localityTypeModels();
            List<StreetsModel> requestModelList = streetsDAO.streetsModelList();
            boolean isValid = true;
            int streetType = 0;
            int localityType = 0;
            for (StreetsModel aRequestModelList : requestModelList) {
                if (aRequestModelList.getName().equals(streetModel.getName().trim()))
                    isValid = false;
            }
            for (StreetTypeModel aStreetTypeModelList : streetTypeModelList) {
                if (aStreetTypeModelList.getName().equalsIgnoreCase(streetModel.getStreetTypeName()))
                    streetType = aStreetTypeModelList.getStreetTypeId();
            }
            for (LocalityTypeModel localityTypeModel : localityTypeModelList) {
                if (localityTypeModel.getLocalityName().equalsIgnoreCase(streetModel.getLocalityTypeName()))
                    localityType = localityTypeModel.getLocalityTypeId();
            }

            if (isValid) {
                UpdateTablesDAO.saveOrUpdate(streetsDAO.getJdbcTemplate()
                        , new Object[]{null, streetModel.getName().trim(), streetType, localityType}
                        , "INSERT INTO `streets`(`streetId`, `name`, `streetTypeId`, `localityId`)  VALUES (?,?,?,?)");
                modelAndView.setViewName("success");
                modelAndView.addObject("message", new CallbackModel("Добавление прошло успешно"));
            } else {
                modelAndView.setViewName("error");
                modelAndView.addObject("message", new CallbackModel("Такая улица уже существует"));
            }
        } else {
            modelAndView.setViewName("error");
            modelAndView.addObject("message", new CallbackModel("Неправильно введеные данные"));
        }
        return modelAndView;
    }


    @RequestMapping(value = "addNewSupplier")
    public ModelAndView addNewLegalSuppler(@ModelAttribute("supplier") NewSupplierInfoModel model) {
        ModelAndView modelAndView = new ModelAndView();
        if (model.isEntity()) {
            if (!model.getIndividualSuppliersModel().isValid()) {
                modelAndView.setViewName("error");
                modelAndView.addObject("message", new CallbackModel("Неправильно введеные данные"));

                return modelAndView;
            }
        } else {
            if (!model.getLegalSuppliersModel().isValid()) {
                modelAndView.setViewName("error");
                modelAndView.addObject("message", new CallbackModel("Неправильно введеные данные"));
                return modelAndView;
            }
        }
        addSupplier(model);
        modelAndView.setViewName("success");
        modelAndView.addObject("message", new CallbackModel("Успешно добавлено"));
        return modelAndView;

    }

    public ModelAndView addSupplier(NewSupplierInfoModel model) {
        ModelAndView modelAndView = new ModelAndView();
        if (isSupplierValid(model)) {
            List<StreetsModel> requestModelList = streetsDAO.streetsModelList();
            List<TelephoneTypeModel> telephoneTypeModelList = telephoneTypeDAO.telephoneTypeModelList();
            int telephoneTypeId = 0;
            int streetId = 0;

            for (int i = 0; i < requestModelList.size(); i++) {
                if (requestModelList.get(i).getName().equalsIgnoreCase(model.getStreet()))
                    streetId = requestModelList.get(i).getStreetId();
            }
            for (int i = 0; i < telephoneTypeModelList.size(); i++) {
                if (telephoneTypeModelList.get(i).getName().equalsIgnoreCase(model.getTelephoneCode()))
                    telephoneTypeId = telephoneTypeModelList.get(i).getTelephoneTypeId();
            }

            UpdateTablesDAO.saveOrUpdate(streetsDAO.getJdbcTemplate()
                    , new Object[]{null, model.getPostCode(), streetId, model.getBuildNum()}
                    , "INSERT INTO `address`(`addressId`, `postcode`, `streetId`, `buildingAddress`) " +
                            "VALUES(?,?,?,?)");

            int addressId = 0;
            List<AddressModel> addressModels = addressDao.addressList();
            for (int i = 0; i < addressModels.size(); i++) {
                if (addressModels.get(i).getBuildingAddress().equals(model.getBuildNum())) {
                    addressId = addressModels.get(i).getAddressId();
                    break;
                }
            }

            UpdateTablesDAO.saveOrUpdate(streetsDAO.getJdbcTemplate()
                    , new Object[]{null, addressId, telephoneTypeId, model.getTelephoneNumber()}
                    , "INSERT INTO `suppliers`(`suppliersId`, `addressId`, `telephoneTypeId`, `phone`) VALUES " +
                            "(?,?,?,?)");

            int supplierId = 0;
            List<SuppliersModel> suppliersModels = suppliersDAO.suppliersModelList();
            for (int i = 0; i < suppliersModels.size(); i++) {
                if (suppliersModels.get(i).getAddressId() == addressId)
                    supplierId = suppliersModels.get(i).getSuppliersId();
            }

            if (model.isEntity()) {
                UpdateTablesDAO.saveOrUpdate(streetsDAO.getJdbcTemplate()
                        , new Object[]{supplierId, model.getIndividualSuppliersModel().getDocumentId()
                                , model.getIndividualSuppliersModel().getName(), model.getIndividualSuppliersModel().getSurname(),
                                model.getIndividualSuppliersModel().getPatronymic()}
                        , "INSERT INTO `individualSuppliers`(`suppliersId`, `documentId`, `name`, `surname`, `patronymic`) VALUES" +
                                "(?,?,?,?,?)");
                modelAndView.setViewName("success");
                modelAndView.addObject("message", new CallbackModel("Добавление прошло успешно"));
                return modelAndView;

            } else {
                UpdateTablesDAO.saveOrUpdate(streetsDAO.getJdbcTemplate()
                        , new Object[]{supplierId, model.getLegalSuppliersModel().getName()
                                , model.getLegalSuppliersModel().getTaxNumer(), model.getLegalSuppliersModel().getCertificateNumber()}
                        , "INSERT INTO `legalSuppliers`(`suppliersId`, `name`, `taxNumber`, `certificateNumber`) VALUES" +
                                "(?,?,?,?)");
                modelAndView.setViewName("success");
                modelAndView.addObject("message", new CallbackModel("Добавление прошло успешно"));
                return modelAndView;
            }


        } else {
            modelAndView.setViewName("error");
            modelAndView.addObject("message", new CallbackModel("Неправильно введены данные"));

        }
        return modelAndView;
    }


    @RequestMapping(value = "addManufacturer")
    public ModelAndView addManufacturer() {
        ModelAndView model = new ModelAndView("admin/addManufacturerForm");
        List<String> countryList = getCountryList();
        List<String> regionList = getRegionList();
        List<String> localityList = getLocalityList();
        List<String> streetList = getStreetList();
        List<String> telephoneTypeList = getTelephoneType();

        model.addObject("countryList", countryList);
        model.addObject("regionList", regionList);
        model.addObject("localityList", localityList);
        model.addObject("streetList", streetList);
        model.addObject("telephoneTypeList", telephoneTypeList);

        model.addObject("manufacturer", new ManufacturerModel());
        model.setViewName("admin/addManufacturerForm");
        return model;
    }

    @Autowired
    ManufacturerDAO manufacturerDAO;

    @RequestMapping(value = "addM")
    public ModelAndView add(@ModelAttribute(value = "manufacturer") ManufacturerModel model) {
        if (model.isValid()) {
            if (isManufacturerValid(model)) {
                List<StreetsModel> requestModelList = streetsDAO.streetsModelList();
                List<ManufacturerModel> manufacturerModelList = manufacturerDAO.manufacturerModelList();
                for (int i = 0; i < manufacturerModelList.size();i++) {
                    if (manufacturerModelList.get(i).getManufacturerName().equalsIgnoreCase(model.getManufacturerName().trim())) {
                        ModelAndView modelAndView = new ModelAndView("error");
                        modelAndView.addObject("message", new CallbackModel("Производитель с таким именем существует"));
                        return modelAndView;
                    }

                }
                int streetId = 0;

                for (int i = 0; i < requestModelList.size(); i++) {
                    if (requestModelList.get(i).getName().equalsIgnoreCase(model.getStreet()))
                        streetId = requestModelList.get(i).getStreetId();
                }

                UpdateTablesDAO.saveOrUpdate(streetsDAO.getJdbcTemplate()
                        , new Object[]{null, model.getPostCode(), streetId, model.getBuildNum()}
                        , "INSERT INTO `address`(`addressId`, `postcode`, `streetId`, `buildingAddress`) " +
                                "VALUES(?,?,?,?)");

                int addressId = 0;
                List<AddressModel> addressModels = addressDao.addressList();
                for (int i = 0; i < addressModels.size(); i++) {
                    if (addressModels.get(i).getBuildingAddress().equals(model.getBuildNum())) {
                        addressId = addressModels.get(i).getAddressId();
                        break;
                    }
                }

                UpdateTablesDAO.saveOrUpdate(streetsDAO.getJdbcTemplate()
                        , new Object[]{null, model.getManufacturerName(), addressId, model.getContactPerson(), model.getTelephoneNumber()}
                        , "INSERT INTO `manufacturers`(`manufacturerId`, `manufacturerName`, `addressId`, `contactPerson`, `telephone`)" +
                                " VALUES (?,?,?,?,?)");
                ModelAndView modelAndView = new ModelAndView("success");
                modelAndView.addObject("message", new CallbackModel("Добавление прошло успешно"));
                return modelAndView;

            } else {
                ModelAndView modelAndView = new ModelAndView("error");
                modelAndView.addObject("error", new CallbackModel("По данным данным нельзя добавить адрес"));
                return modelAndView;

            }
        } else {
            ModelAndView modelAndView = new ModelAndView("error");
            modelAndView.addObject("error", new CallbackModel("неверно введены данные"));
            return modelAndView;
        }
    }

    public boolean isSupplierValid(NewSupplierInfoModel model) {
        boolean isValid = true;
        if (!model.isEntity()) {
            List<LegalSuppliersModel> legalSuppliersModelList = legalSuppliersDAO.legalSuppliersModels();
            for (int i = 0; i < legalSuppliersModelList.size(); i++) {
                if (legalSuppliersModelList.get(i).getCertificateNumber()
                        .equalsIgnoreCase(model.getLegalSuppliersModel().getCertificateNumber().trim())
                        || legalSuppliersModelList.get(i).getTaxNumer().equalsIgnoreCase(model.getLegalSuppliersModel().getTaxNumer().trim()))
                    isValid = false;
            }
            if (model.getBuildNum().isEmpty() || model.getPostCode().isEmpty() || model.getTelephoneNumber().isEmpty())
                isValid = false;
        } else {
            List<IndividualSuppliersModel> individualSuppliersModelList = individualSuppliersDAO.individualSuppliersModels();
            for (int i = 0; i < individualSuppliersModelList.size(); i++) {
                if (individualSuppliersModelList.get(i).getDocumentId()
                        .equalsIgnoreCase(model.getIndividualSuppliersModel().getDocumentId().trim()))
                    isValid = false;
            }
            if (model.getBuildNum().isEmpty() || model.getPostCode().isEmpty() || model.getTelephoneNumber().isEmpty())
                isValid = false;
        }
        return isValid;
    }

    public boolean isManufacturerValid(ManufacturerModel model) {
        List<CountryesModel> countryesModelList = countryesDao.addressList();
        List<RegionModel> regionModelList = regionDAO.regionModels();
        List<LocalityModel> localityModelList = localityDAO.localityModels();
        List<StreetsModel> streetsModelList = streetsDAO.streetsModelList();
        int countryId = 0;
        int regionId = 0;
        int localityId = 0;
        int streetId = 0;
        boolean isValid = false;
        for (int i = 0; i < countryesModelList.size(); i++) {
            if (model.getCountry().equalsIgnoreCase(countryesModelList.get(i).getName())) {
                countryId = countryesModelList.get(i).getCountryId();
            }
        }

        for (int i = 0; i < regionModelList.size(); i++) {
            if (model.getRegion().equalsIgnoreCase(regionModelList.get(i).getName())) {
                regionId = countryesModelList.get(i).getCountryId();
            }
        }

        for (int i = 0; i < regionModelList.size(); i++) {
            if (regionModelList.get(i).getRegionId() == regionId) {
                if (regionModelList.get(i).getCountryId() == countryId)
                    isValid = true;
            }
        }
        if (!isValid)
            return false;

        isValid = false;
        for (int i = 0; i < localityModelList.size(); i++) {
            if (model.getLocality().equalsIgnoreCase(localityModelList.get(i).getName())) {
                localityId = localityModelList.get(i).getLocalityId();
            }
        }

        for (int i = 0; i < localityModelList.size(); i++) {
            if (localityModelList.get(i).getLocalityId() == localityId) {
                if (localityModelList.get(i).getRegionId() == regionId)
                    isValid = true;
            }
        }
        if (!isValid)
            return false;

        isValid = false;

        for (int i = 0; i < streetsModelList.size(); i++) {
            if (model.getStreet().equalsIgnoreCase(streetsModelList.get(i).getName())) {
                streetId = streetsModelList.get(i).getStreetId();
            }
        }
        for (int i = 0; i < localityModelList.size(); i++) {
            if (streetsModelList.get(i).getStreetId() == streetId) {
                if (streetsModelList.get(i).getLocaityTypeId() == localityId)
                    isValid = true;
            }
        }
        if (!isValid)
            return false;

        return true;

    }

    @RequestMapping(value = "acceptanceOfGoodsNextForm")
    public ModelAndView acceptanceOfGoodsNext(ModelAndView model, @ModelAttribute("supplier") DropDownValuesModel dModel
            , BindingResult res) {
        DropDownValidator validator = new DropDownValidator();
        validator.validate(dModel, res);
        if (res.hasErrors()) {
            model.addObject("supplierLists", SuppliersModel.getNameList(
                    suppliersDAO.suppliersModelList(), individualSuppliersDAO.individualSuppliersModels(), legalSuppliersDAO.legalSuppliersModels()));
            model.setViewName("/admin/acceptanceOfGoodsForm");
        } else {
            model.addObject("result", dModel);
            model.setViewName("/admin/selectSupplyContractForm");
        }
        return model;
    }

    @RequestMapping(value = "addNewLocality")
    public ModelAndView addNewLocality(@ModelAttribute("locality") LocalityModel localityModel) {
        ModelAndView modelAndView = new ModelAndView();
        if (localityModel.isValid()) {
            List<RegionModel> regionModelList = regionDAO.regionModels();
            List<LocalityTypeModel> localityTypeModelList = localityTypeDAO.localityTypeModels();
            boolean isValid = true;
            int regionType = 0;
            int localityType = 0;
            for (RegionModel regionModel : regionModelList) {
                if (regionModel.getName().equals(localityModel.getName().trim()))
                    isValid = false;
                if (regionModel.getName().equalsIgnoreCase(localityModel.getRegionName()))
                    regionType = regionModel.getRegionId();
            }

            for (LocalityTypeModel localityTypeModel : localityTypeModelList) {
                if (localityTypeModel.getLocalityName().equalsIgnoreCase(localityModel.getLocalityTypeName()))
                    localityType = localityTypeModel.getLocalityTypeId();
            }

            if (isValid) {
                UpdateTablesDAO.saveOrUpdate(streetsDAO.getJdbcTemplate()
                        , new Object[]{null, localityModel.getTelephoneCode(), localityModel.getName(), regionType, localityType}
                        , "INSERT INTO `locality`(`localityId`, `telephoneCode`, `name`, `regionId`, `localityTypeId`)" +
                                " VALUES(?,?,?,?,?)");
                modelAndView.setViewName("success");
                modelAndView.addObject("message", new CallbackModel("Добавление прошло успешно"));

            } else {
                modelAndView.setViewName("error");
                modelAndView.addObject("message", new CallbackModel("Такой населенный пункт уже существует"));
            }
        } else {
            modelAndView.setViewName("error");
            modelAndView.addObject("message", new CallbackModel("Неправильно введены данные"));
        }
        return modelAndView;
    }

    public List<String> getCountryList() {
        List<CountryesModel> countryList = countryesDao.addressList();
        List<String> resList = new ArrayList<>();
        for (CountryesModel aCountryList : countryList) {
            resList.add(aCountryList.getName());
        }
        return resList;
    }

    public List<String> getRegionList() {
        List<RegionModel> regionModelList = regionDAO.regionModels();
        List<String> resList = new ArrayList<>();
        for (RegionModel aRegionModelList : regionModelList) {
            resList.add(aRegionModelList.getName());
        }
        return resList;
    }

    //todo пофиксить пффд

    public List<String> getLocalityList() {
        List<LocalityModel> localityModelList = localityDAO.localityModels();
        List<String> resList = new ArrayList<>();
        for (LocalityModel localityModel : localityModelList) {
            resList.add(localityModel.getName());
        }
        return resList;
    }

    public List<String> getStreetList() {
        List<StreetsModel> streetsModels = streetsDAO.streetsModelList();
        List<String> resList = new ArrayList<>();
        for (StreetsModel streetsModel : streetsModels) {
            resList.add(streetsModel.getName());
        }
        return resList;
    }

    public List<String> getTelephoneType() {
        List<TelephoneTypeModel> telephoneTypeModelList = telephoneTypeDAO.telephoneTypeModelList();
        List<String> resList = new ArrayList<>();
        for (TelephoneTypeModel telephoneTypeModel : telephoneTypeModelList) {
            resList.add(telephoneTypeModel.getName());
        }
        return resList;
    }

    public List<String> getLocalityTypeList() {
        List<LocalityTypeModel> localityTypeModelList = localityTypeDAO.localityTypeModels();
        List<String> resList = new ArrayList<>();
        for (LocalityTypeModel localityTypeModel : localityTypeModelList) {
            resList.add(localityTypeModel.getLocalityName());
        }
        return resList;
    }
}

