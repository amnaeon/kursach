package net.codejava.spring.controller;

import net.codejava.spring.Viewer;
import net.codejava.spring.consts.Const;
import net.codejava.spring.dao.*;
import net.codejava.spring.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class TableShowerController {
    @Autowired
    private AddressDao addressDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ByerDAO byerDAO;

    @Autowired
    private CountryesDao countryesDao;


    @Autowired
    private DeliveredGoodsDAO deliveredGoodsDAO;


    @Autowired
    private GoodsCategoryDAO goodsCategoryDAO;

    @Autowired
    private IndividualSuppliersDAO individualSuppliersDAO;

    @Autowired
    private LegalSuppliersDAO legalSuppliersDAO;

    @Autowired
    private LegalByerDAO legalByerDAO;


    @Autowired
    private LocalityDAO localityDAO;

    @Autowired
    private LocalityTypeDAO localityTypeDAO;

    @Autowired
    private ManufacturerDAO manufacturerDAO;

    @Autowired
    private RegionDAO regionDAO;

    @Autowired
    private SalesContractDAO salesContractDAO;

    @Autowired
    private SoldGoodsDAO soldGoodsDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private StreetsDAO streetsDAO;

    @Autowired
    private StreetsTypeDAO streetsTypeDAO;

    @Autowired
    private SuppliersDAO suppliersDAO;

    @Autowired
    private SupplyContractDAO supplyContractDAO;

    @Autowired
    private TelephoneTypeDAO telephoneTypeDAO;

    @RequestMapping(value = "/showAddress")
    public ModelAndView showAdress(ModelAndView model) {
        List<AddressModel> addressModelList = addressDao.addressList();
        model.addObject(addressModelList);
        model.setViewName("tables/address");
        return model;
    }

    @RequestMapping(value = "/showProduct")
    public ModelAndView showProduct(ModelAndView model) {
        List<ProductModel> productModelList = productDao.productList();
        model.addObject(productModelList);
        model.setViewName("tables/product");
        return model;
    }

    @RequestMapping(value = "/showStreetType")
    public ModelAndView showStreetType(ModelAndView model) {
        List<StreetTypeModel> streetTypeList = streetsTypeDAO.streetTypeList();
        model.addObject(streetTypeList);
        model.setViewName("tables/streetType");
        return model;
    }

    @RequestMapping(value = "/showByers")
    public ModelAndView showAddress(ModelAndView model) {
        List<ByerModel> byerModelList = byerDAO.byerList();
        model.addObject(byerModelList);
        model.setViewName("tables/byer");
        return model;
    }

    @RequestMapping(value = "/showCountyes")
    public ModelAndView showCountyes(ModelAndView model) {
        List<CountryesModel> countryModelList = countryesDao.addressList();
        model.addObject("countryModelList",countryModelList);
        model.setViewName("tables/countryes");
        return model;
    }

    @RequestMapping(value = "/showDeliveredGoods")
    public ModelAndView showDeliveredGoods(ModelAndView model) {
        List<DeliveredGoodsModel> deliveredGoodsModelList = deliveredGoodsDAO.deliveredGoodsModelList();
        model.addObject(deliveredGoodsModelList);
        model.setViewName("tables/deliveredGoods");
        return model;
    }


    @RequestMapping(value = "/showGoodsCategory")
    public ModelAndView showGoodsCategory(ModelAndView model) {
        List<GoodsCategoryModel> goodsCategoryModelList = goodsCategoryDAO.goodsCategoryModels();
        model.addObject(goodsCategoryModelList);
        model.setViewName("tables/goodsCategory");
        return model;
    }

    @RequestMapping(value = "/showIndividualSuppliers")
    public ModelAndView showIndividualSuppliers(ModelAndView model) {
        List<IndividualSuppliersModel> individualSuppliersModelList = individualSuppliersDAO.individualSuppliersModels();
        model.addObject(individualSuppliersModelList);
        model.setViewName("tables/individualSuppliers");
        return model;
    }

    @RequestMapping(value = "/showLegalByer")
    public ModelAndView showLegalByer(ModelAndView model) {
        List<LegalByerModel> legalByerModelList = legalByerDAO.legalByerModels();
        model.addObject(legalByerModelList);
        model.setViewName("tables/legalByer");
        return model;
    }

    @RequestMapping(value = "/showLegalSuppliers")
    public ModelAndView showLegalSuppliers(ModelAndView model) {
        List<LegalSuppliersModel> legalSuppliersModelList = legalSuppliersDAO.legalSuppliersModels();
        model.addObject(legalSuppliersModelList);
        model.setViewName("tables/legalSuppliers");
        return model;
    }

    @RequestMapping(value = "/showLocality")
    public ModelAndView showLocality(ModelAndView model) {
        List<LocalityModel> localityModelList = localityDAO.localityModels();
        model.addObject(localityModelList);
        model.setViewName("tables/locality");
        return model;
    }

    @RequestMapping(value = "/showLocalityType")
    public ModelAndView showLocalityType(ModelAndView model) {
        List<LocalityTypeModel> localityTypeModelList = localityTypeDAO.localityTypeModels();
        model.addObject(localityTypeModelList);
        model.setViewName("tables/localityType");
        return model;
    }

    @RequestMapping(value = "/showManufacturer")
    public ModelAndView showManufacturer(ModelAndView model) {
        List<ManufacturerModel> manufacturerModelList = manufacturerDAO.manufacturerModelList();
        model.addObject(manufacturerModelList);
        model.setViewName("tables/manufacturer");
        return model;
    }
    @RequestMapping(value = "/showRegion")
    public ModelAndView showRegion(ModelAndView model) {
        List<RegionModel> regionModelList = regionDAO.regionModels();
        model.addObject(regionModelList);
        model.setViewName("tables/region");
        return model;
    }

    @RequestMapping(value = "/showSalesContract")
    public ModelAndView showSalesContract(ModelAndView model) {
        List<SalesContractsModel> salesContractsModelList = salesContractDAO.contractsModelList();
        model.addObject(salesContractsModelList);
        model.setViewName("tables/salesContract");
        return model;
    }

    @RequestMapping(value = "/showSoldGoods")
    public ModelAndView showSoldGoods(ModelAndView model) {
        List<SoldGoodsModel> soldGoodsModelList = soldGoodsDAO.soldGoodsModels();
        model.addObject(soldGoodsModelList);
        model.setViewName("tables/soldGoods");
        return model;
    }

    @RequestMapping(value = "/showStock")
    public ModelAndView showStock(ModelAndView model) {
        List<StockModel> stockModelList = stockDAO.stockModels();
        model.addObject(stockModelList);
        model.setViewName("tables/stock");
        return model;
    }

    @RequestMapping(value = "/showStreets")
    public ModelAndView showStreets(ModelAndView model) {
        List<StreetsModel> streetsModelList = streetsDAO.streetsModelList();
        model.addObject(streetsModelList);
        model.setViewName("tables/streets");
        return model;
    }

    @RequestMapping(value = "/showSuppliers")
    public ModelAndView showSuppliers(ModelAndView model) {
        List<SuppliersModel> suppliersModelList = suppliersDAO.suppliersModelList();
        model.addObject(suppliersModelList);
        model.setViewName("tables/suppliers");
        return model;
    }

    @RequestMapping(value = "/showContract")
    public ModelAndView showContract(ModelAndView model) {
        List<SupplyContractModel> supplyContractModelList = supplyContractDAO.supplyContractModelList();
        model.addObject(supplyContractModelList);
        model.setViewName("tables/supplyContact");
        return model;
    }


    @RequestMapping(value = "/showTelephoneType")
    public ModelAndView showTelephoneType(ModelAndView model) {
        List<TelephoneTypeModel> telephoneTypeModelList = telephoneTypeDAO.telephoneTypeModelList();
        model.addObject(telephoneTypeModelList);
        model.setViewName("tables/telephoneType");
        return model;
    }
}
