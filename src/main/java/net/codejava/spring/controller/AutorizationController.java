package net.codejava.spring.controller;

import net.codejava.spring.consts.Const;
import net.codejava.spring.dao.UserDAO;
import net.codejava.spring.model.CallbackModel;
import net.codejava.spring.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class AutorizationController {
    @Autowired
    private UserDAO userDAO;
    @RequestMapping(value = "/")
    public ModelAndView main() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", new User());
        modelAndView.setViewName("autorization");
        return modelAndView;
    }

    @RequestMapping(value = "/check-user")
    public ModelAndView checkUser(@ModelAttribute("user") @Valid User user, BindingResult result) {
        ModelAndView modelAndView = new ModelAndView();
        if(result.hasErrors()) {
            return new ModelAndView("autorization");
        }
        user.setTypeFomList(userDAO.userList(Const.User.GET_ALL_USER_TYPE));
        if (user.getType().equals(Const.User.ADMIN))
            modelAndView.setViewName("adminFrom");
        else {
            if (user.getType().equals(Const.User.USER))
                modelAndView.setViewName("studForm");
            else {
                modelAndView.setViewName("error");
                modelAndView.addObject("message",new CallbackModel("Неверно введены данные"));
            }
        }
        return modelAndView;
    }
}

