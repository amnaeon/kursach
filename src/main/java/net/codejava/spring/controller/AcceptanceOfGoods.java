package net.codejava.spring.controller;

import net.codejava.spring.dao.*;
import net.codejava.spring.dao.baseDAOs.UpdateTablesDAO;
import net.codejava.spring.managers.AddProductManager;
import net.codejava.spring.managers.ProductManager;
import net.codejava.spring.managers.SuppliresManager;
import net.codejava.spring.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by stus on 02.05.17.
 */
@Controller
public class AcceptanceOfGoods {
    @Autowired
    SupplyContractDAO supplyContractDAO;
    @Autowired
    SuppliersDAO suppliersDAO;
    @Autowired
    GoodsCategoryDAO goodsCategoryDAO;
    @Autowired
    ManufacturerDAO manufacturerDAO;
    @Autowired
    ProductDao productDao;
    @Autowired
    StockDAO stockDAO;
    @Autowired
    DeliveredGoodsDAO deliveredGoodsDAO;
    @Autowired
    IndividualSuppliersDAO individualSuppliersDAO;
    @Autowired
    LegalSuppliersDAO legalSuppliersDAO;

    @RequestMapping(value = "acceptanceOfGood")
    public ModelAndView acceptanceOfGood(@ModelAttribute(value = "data") DropDownValuesModel model) {
        ModelAndView modelAndView = new ModelAndView();
        SupplyContractModel supplyContractModel = new SupplyContractModel();
        List<String> data = new ArrayList<>();
        List<SupplyContractModel> supplyContractModelList = supplyContractDAO.supplyContractModelList();
        List<SuppliersModel> suppliersModelList = suppliersDAO.suppliersModelList();
        suppliersModelList = SuppliersModel.setName(suppliersModelList, individualSuppliersDAO.individualSuppliersModels(), legalSuppliersDAO.legalSuppliersModels());
        int suppliersId = 0;
        for (int i = 0; i < suppliersModelList.size(); i++) {
            if (suppliersModelList.get(i).getName().equals(model.getdName().trim()))
                suppliersId = suppliersModelList.get(i).getSuppliersId();
        }


        for (int i = 0; i < supplyContractModelList.size(); i++) {
            if (supplyContractModelList.get(i).getSuppliersId() == suppliersId)
                data.add(supplyContractModelList.get(i).getSupplyContractId());
        }

        SuppliresManager.getInstance().setSuppliersId(suppliersId);
        modelAndView.addObject("dataList", data);
        modelAndView.addObject("sId", suppliersId);
        modelAndView.addObject("supplyContract", supplyContractModel);
        modelAndView.setViewName("admin/acceptanceOfGoodsSelector");
        return modelAndView;
    }

    @RequestMapping(value = "addNewSupplyContractBtn")
    public ModelAndView addNewSupplyContractBtn(@ModelAttribute("supplyContract") SupplyContractModel model) {
        ModelAndView modelAndView = new ModelAndView("admin/addNewSupplyContract");
        modelAndView.addObject("model", model);
        return modelAndView;
    }

    @RequestMapping(value = "addNewSupplyContract", method = RequestMethod.POST)
    public ModelAndView addNewSupplyContract(@ModelAttribute("supplyContract") SupplyContractModel model) {
        ModelAndView modelAndView = new ModelAndView("admin/addProductNextStep");
        if (model.isValid()) {
            AddProductManager.getInstance().setSupplyContractModel(model);
            modelAndView.addObject("product", AddProductManager.getInstance().getAddProductModelList());
            modelAndView.addObject("addProductModel", new AddProductModel());


        } else {
            modelAndView = new ModelAndView("error");
            modelAndView.addObject("message", new CallbackModel("Данные введены не верно"));
        }
        return modelAndView;
    }

    @RequestMapping(value = "selectSupplyContract")
    public ModelAndView selectSupplyContract(@ModelAttribute("supplyContract") SupplyContractModel model) {
        SuppliresManager.getInstance().setProductModelList(new ArrayList<AddProductModel>());
        ModelAndView modelAndView = new ModelAndView("admin/goodsSelector");


        List<AddProductModel> productList = new ArrayList<>();
        List<ProductModel> productModelList = productDao.productList();
        List<GoodsCategoryModel> goodsCategoryModelList = goodsCategoryDAO.goodsCategoryModels();
        List<ManufacturerModel> manufacturerModelList = manufacturerDAO.manufacturerModelList();
        List<StockModel> stockModelList = stockDAO.stockModels();
        List<DeliveredGoodsModel> deliveredGoodsModelList = deliveredGoodsDAO.deliveredGoodsModelList();
        for (int k = 0; k < deliveredGoodsModelList.size(); k++) {
            if (deliveredGoodsModelList.get(k).getSupplyContractId().equals(model.getSupplyContractId())) {
                String categoryName = "";
                String manufacturerName = "";
                for (int j = 0; j < goodsCategoryModelList.size(); j++) {
                    if (deliveredGoodsModelList.get(k).getCategoryCode() == (goodsCategoryModelList.get(j).getCategoryId())) {
                        categoryName = goodsCategoryModelList.get(j).getCategoryName();
                        break;
                    }
                }

                for (int j = 0; j < manufacturerModelList.size(); j++) {
                    if (deliveredGoodsModelList.get(k).getManufacturerId() == (manufacturerModelList.get(j).getManufacturerId())) {
                        manufacturerName = manufacturerModelList.get(j).getManufacturerName();
                        break;
                    }
                }

                AddProductModel addProductModel = new AddProductModel();
                addProductModel.setName(deliveredGoodsModelList.get(k).getName());
                addProductModel.setModel(deliveredGoodsModelList.get(k).getModel());
                addProductModel.setCategoryName(categoryName);
                addProductModel.setManufacturerName(manufacturerName);
                addProductModel.setСount(deliveredGoodsModelList.get(k).getCount());
                addProductModel.setPrice(deliveredGoodsModelList.get(k).getPrice());
                productList.add(addProductModel);
                SuppliresManager.getInstance().setSuppliersContract(model.getSupplyContractId());
                SuppliresManager.getInstance().setProductModelList(productList);
            }
        }

        if (productList.size() == 0) {
            ModelAndView modelAndView1 = new ModelAndView("error");
            modelAndView1.addObject("message", new CallbackModel("Добавьте договор о поставках"));
            return modelAndView1;
        }
        modelAndView.addObject("productList", new NewAddProductModel(productList, ""));

        return modelAndView;
    }


    public List<String> getCategoryList() {
        List<GoodsCategoryModel> goodsCategoryModelList = goodsCategoryDAO.goodsCategoryModels();
        List<String> res = new ArrayList<>();
        for (int i = 0; i < goodsCategoryModelList.size(); i++) {
            res.add(goodsCategoryModelList.get(i).getCategoryName());
        }
        return res;
    }

    @RequestMapping(value = "addProduct")
    public ModelAndView addProduct(@ModelAttribute(value = "product") AddProductModel productModel) {
        ModelAndView modelAndView = new ModelAndView();
        if (productModel.isValid()) {
            for (int i = 0; i < AddProductManager.getInstance().getAddProductModelList().size(); i++) {
                if (productModel.getName().trim().equalsIgnoreCase(AddProductManager.getInstance().getAddProductModelList().get(i).getName().trim()) &&
                        productModel.getModel().trim().equalsIgnoreCase(AddProductManager.getInstance().getAddProductModelList().get(i).getModel())) {
                    modelAndView.addObject("message", new CallbackModel("Такой товар уже существует"));
                    modelAndView.setViewName("error");
                    return modelAndView;
                }

            }
            AddProductManager.getInstance().getAddProductModelList().add(productModel);
            modelAndView = addNewSupplyContract(AddProductManager.getInstance().getSupplyContractModel());
        } else {
            modelAndView.addObject("message", new CallbackModel("Неверно введены данные"));
            modelAndView.setViewName("error");
        }
        return modelAndView;
    }

    @RequestMapping(value = "createContract")
    public ModelAndView createContract() {
        List<AddProductModel> addProductModelList = AddProductManager.getInstance().getAddProductModelList();
        SupplyContractModel model = AddProductManager.getInstance().getSupplyContractModel();
        int categoryId = 0;
        int manufacturerId = 0;
        if (AddProductManager.getInstance().getAddProductModelList().size() == 0) {
            ModelAndView modelAndView = new ModelAndView("error");
            modelAndView.addObject("message", new CallbackModel("Договор не может быть пустым"));
            return modelAndView;
        }
        for (int j = 0; j < addProductModelList.size(); j++) {
            for (int i = 0; i < goodsCategoryDAO.goodsCategoryModels().size(); i++) {
                if (goodsCategoryDAO.goodsCategoryModels().get(i).getCategoryName().equalsIgnoreCase(addProductModelList.get(j).getCategoryName()))
                    categoryId = goodsCategoryDAO.goodsCategoryModels().get(i).getCategoryId();
            }

            for (int i = 0; i < manufacturerDAO.manufacturerModelList().size(); i++) {
                if (manufacturerDAO.manufacturerModelList().get(i).getManufacturerName().equalsIgnoreCase(addProductModelList.get(j).getManufacturerName()))
                    manufacturerId = manufacturerDAO.manufacturerModelList().get(i).getManufacturerId();
            }
        }
        UpdateTablesDAO.saveOrUpdate(supplyContractDAO.getJdbcTemplate()
                , new Object[]{model.getSupplyContractId(), model.getCouclusionDate(), model.getExpirationDate()
                        , model.getDeliveryDate(), SuppliresManager.getInstance().getSuppliersId()}
                , "INSERT INTO `supplyСontracts`(`supplyContranctId`, `conclusionDate`, `expirationDate`, `deliveryDate`, `suppliersId`)" +
                        " VALUES(?,?,?,?,?)");

        for (int i = 0; i < addProductModelList.size(); i++) {


            UpdateTablesDAO.saveOrUpdate(supplyContractDAO.getJdbcTemplate()
                    , new Object[]{model.getSupplyContractId(), addProductModelList.get(i).getName(), addProductModelList.get(i).getModel()
                            , categoryId, manufacturerId, Float.parseFloat(addProductModelList.get(i).getPrice()), Float.parseFloat(addProductModelList.get(i).getСount())}
                    , "INSERT INTO `deliveredGoods`(`supplyContractId`, `name`, `model`, `categoryId`, `manufacturerId`, `price`, `count`)" +
                            " VALUES(?,?,?,?,?,?,?)");
            SuppliresManager.getInstance().setSuppliersId(0);
            AddProductManager.getInstance().setAddProductModelList(new ArrayList<AddProductModel>());
        }
        return new ModelAndView("adminFrom");

    }

    @RequestMapping(value = "goodsSelectorCansell")
    public ModelAndView cancell() {
        return new ModelAndView("showAllTableForm");
    }

    @RequestMapping(value = "supplyGoods")
    public ModelAndView supplyGoods(@ModelAttribute(value = "productList") NewAddProductModel model) {
        List<AddProductModel> addProductModelList = SuppliresManager.getInstance().getProductModelList();
        List<ProductModel> productModelList = productDao.productList();
        boolean isChange = false;
        for (int j = 0; j < productModelList.size(); j++) {
            if (productModelList.get(j).getSerialNumber().equalsIgnoreCase(model.getName())) {
                if (productModelList.get(j).getAvalibilityCode() == 2) {
                    ModelAndView modelAndView = new ModelAndView("error");
                    modelAndView.addObject("message", new CallbackModel("Данный товар уже оприходован"));
                    return modelAndView;
                }
                UpdateTablesDAO.saveOrUpdate(productDao.getJdbcTemplate(), new Object[]{},
                        "UPDATE product SET availabilityCode = 2"
                                + " WHERE serialNumber = '"
                                + model.getName() + "'");
                SuppliresManager.getInstance().removeBySerialNum(model.getName());
                isChange = true;
                break;
            }

        }
        if (!isChange) {
            ModelAndView modelAndView = new ModelAndView("error");
            modelAndView.addObject("message", new CallbackModel("Неверно введен серийный номер"));
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("success");
            modelAndView.addObject("message", new CallbackModel("Товар усшено оприходован"));
            return modelAndView;
        }
    }


    @RequestMapping(value = "addNewProduct")
    public ModelAndView addNewProduct() {
        ModelAndView modelAndView = new ModelAndView("admin/addProduct");
        modelAndView.addObject("product", new AddProductModel());
        List<GoodsCategoryModel> goodsCategoryModelList = goodsCategoryDAO.goodsCategoryModels();
        List<String> categoryList = new ArrayList<>();
        List<ManufacturerModel> manufacturerModelList = manufacturerDAO.manufacturerModelList();
        List<String> manufacturerList = new ArrayList<>();

        for (int i = 0; i < goodsCategoryModelList.size(); i++) {
            categoryList.add(goodsCategoryModelList.get(i).getCategoryName());
        }

        for (int i = 0; i < manufacturerModelList.size(); i++) {
            manufacturerList.add(manufacturerModelList.get(i).getManufacturerName());
        }
        modelAndView.addObject("categoryList", categoryList);
        modelAndView.addObject("manufacturerList", manufacturerList);

        return modelAndView;
    }

    @RequestMapping(value = "deleteContract")
    public ModelAndView deleteContract() {
        SuppliresManager.getInstance().getProductModelList().clear();
        SuppliresManager.getInstance().setSuppliersId(0);
        ProductManager.getInstance().getProductModels().clear();
        goodsCategoryDAO.getJdbcTemplate().update("DELETE FROM `supplyСontracts` WHERE supplyContranctId = ?", SuppliresManager.getInstance().getSuppliersContract());
        SuppliresManager.getInstance().setSuppliersContract("");
        return new ModelAndView("adminFrom");
    }

    public List<String> getManufacturerList() {
        List<ManufacturerModel> manufacturerModelList = manufacturerDAO.manufacturerModelList();
        List<String> res = new ArrayList<>();
        for (int i = 0; i < manufacturerModelList.size(); i++) {
            res.add(manufacturerModelList.get(i).getManufacturerName());
        }
        return res;
    }

    @RequestMapping(value = "startSupply")
    public ModelAndView startSupply(@ModelAttribute(value = "productList") NewAddProductModel model) {
        List<ProductModel> productModelList = new ArrayList<>();
        List<AddProductModel> addProductModelList = SuppliresManager.getInstance().getProductModelList();
        for(int i =0;i<model.getAddProductModelList().size();i++){
            if(!model.isSelected()){
                ModelAndView g = new ModelAndView("error");
                g.addObject("message",new CallbackModel("Отметье весь товар"));
                return g;
            }

        }
        for (int i = 0; i < addProductModelList.size(); i++) {
            ProductModel product = new ProductModel();
            for (int j = 0; j < Integer.parseInt(addProductModelList.get(i).getСount()); j++) {
                product.setName(addProductModelList.get(i).getName());
                product.setModel(addProductModelList.get(i).getModel());
                product.setCategoryName(addProductModelList.get(i).getCategoryName());
                product.setManufacturerName(addProductModelList.get(i).getManufacturerName());
                product.setPrice(addProductModelList.get(i).getPrice());
                productModelList.add(product);

            }

        }
        ProductManager.getInstance().getProductModels().clear();
        ProductManager.getInstance().getProductModels().addAll(productModelList);
        ModelAndView modelAndView = new ModelAndView("s");
        modelAndView.addObject("productList", new Last(productModelList));
        return modelAndView;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @RequestMapping(value = "startSupply1")
    public ModelAndView startSupply1(@ModelAttribute(value = "productList") Last pr) {
        List<ProductModel> productModelList = ProductManager.getInstance().getProductModels();

        String name = pr.getName();
        boolean isChange = false;
        for (int j = 0; j < productModelList.size(); j++) {
            String pName = productModelList.get(j).getName();
            if (customEq(name, pName) && !isChange && pr.getSerialNum().length() > 0 && ProductManager.getInstance().getProductModels().get(j).getSerialNumber() == null) {
                isChange = true;
                ProductModel productModel = new ProductModel();
                productModel.setReleaseDate(pr.getDate());
                productModel.setSerialNumber(pr.getSerialNum());
                productModel.setName(pName);
                productModel.setPrice(productModelList.get(j).getPrice());
                productModel.setCategoryName(productModelList.get(j).getCategoryName());
                productModel.setModel(productModelList.get(j).getModel());
                productModel.setManufacturerName(productModelList.get(j).getManufacturerName());
                ProductManager.getInstance().getProductModels().set(j, productModel);
            }
        }
        ModelAndView modelAndView = new ModelAndView("s");
        modelAndView.addObject("productList", new Last(productModelList));
        if (!isChange) {
            ModelAndView m = new ModelAndView("success");
            m.addObject("message", new CallbackModel("Товар успешно оприходован"));


            for (int i = 0; i < ProductManager.getInstance().getProductModels().size(); i++) {
                ProductModel model = ProductManager.getInstance().getProductModels().get(i);
                int categoryCode = 0;
                int manufacturerCode = 0;
                for (int j = 0; j < goodsCategoryDAO.goodsCategoryModels().size(); j++) {
                    if (goodsCategoryDAO.goodsCategoryModels().get(j).getCategoryName().equalsIgnoreCase(model.getCategoryName()))
                        categoryCode = goodsCategoryDAO.goodsCategoryModels().get(j).getCategoryId();
                }

                for (int j = 0; j < manufacturerDAO.manufacturerModelList().size(); j++) {
                    if (manufacturerDAO.manufacturerModelList().get(j).getManufacturerName().equalsIgnoreCase(model.getManufacturerName()))
                        manufacturerCode = manufacturerDAO.manufacturerModelList().get(j).getManufacturerId();
                }
                try {
                    UpdateTablesDAO.saveOrUpdate(supplyContractDAO.getJdbcTemplate()
                            , new Object[]{model.getSerialNumber(), model.getName(), model.getModel(), model.getReleaseDate(),
                                    2, categoryCode, manufacturerCode, model.getPrice()}
                            , "INSERT INTO `product`(`serialNumber`, `name`, `model`, `releaseDate`, `availabilityCode`, `сategoryCode`, `manufacturerId`, `price`)" +
                                    "VALUES(?,?,?,?,?,?,?,?)");
                } catch (Exception e) {

                }


            }
            ProductManager.getInstance().getProductModels().clear();
            AddProductManager.getInstance().getAddProductModelList().clear();
            AddProductManager.getInstance().setSupplyContractModel(new SupplyContractModel());

            return m;

        }
        return modelAndView;
    }

    public boolean customEq(String a, String b) {
        if (a.length() == b.length()) {
            for (int i = 0; i < a.toCharArray().length; i++) {
                if (a.charAt(i) != b.charAt(i))
                    return false;
            }
        } else return false;
        return true;
    }
}

