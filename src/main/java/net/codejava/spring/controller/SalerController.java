package net.codejava.spring.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import net.codejava.spring.dao.*;
import net.codejava.spring.dao.baseDAOs.UpdateTablesDAO;
import net.codejava.spring.managers.SallerManager;
import net.codejava.spring.managers.SuppliresManager;
import net.codejava.spring.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.text.NumberFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;


import java.util.List;

@Controller
public class SalerController {
    @Autowired
    ProductDao productDao;
    @Autowired
    SoldGoodsDAO soldGoodsDAO;
    @Autowired
    ByerDAO byerDAO;

    @Autowired
    StockDAO stockDAO;
    @Autowired
    ManufacturerDAO manufacturerDAO;
    @Autowired
    GoodsCategoryDAO goodsCategoryDAO;
    @Autowired
    TelephoneTypeDAO telephoneTypeDAO;

    @RequestMapping(value = "sale")
    public ModelAndView sale() {
        ModelAndView modelAndView = new ModelAndView("tables/saleForm");
        modelAndView.addObject("saleData", new SaleData());
        List<ProductModel> productModelList = productDao.productList1();
        modelAndView.addObject("productModelList", productModelList);
        List<String> discountNameList = new ArrayList<>();
        List<ByerModel> byerModelList = byerDAO.byerList();
        List<String> byerNameList = new ArrayList<>();
        List<String> serialNumberList = new ArrayList<>();
        List<StockModel> stockModelList = stockDAO.stockModels();
        List<ManufacturerModel> manufacturerModelList = manufacturerDAO.manufacturerModelList();
        List<GoodsCategoryModel> goodsCategoryModelList = goodsCategoryDAO.goodsCategoryModels();

        for (int i = 0; i < productModelList.size(); i++) {
            if (productModelList.get(i).getAvalibilityCode() != 1 && productModelList.get(i).getAvalibilityCode() != 3) {
                for (int j = 0; j < stockModelList.size(); j++) {
                    if (stockModelList.get(j).getAvailabilityCode() == productModelList.get(i).getAvalibilityCode())
                        productModelList.get(i).setAvalibilityName(stockModelList.get(j).getAvailabilityStatus());
                }
                for (int j = 0; j < manufacturerModelList.size(); j++) {
                    if (manufacturerModelList.get(j).getManufacturerId() == productModelList.get(i).getManufacturerId())
                        productModelList.get(i).setManufacturerName(manufacturerModelList.get(j).getManufacturerName());
                }
                for (int j = 0; j < goodsCategoryModelList.size(); j++) {
                    if (goodsCategoryModelList.get(j).getCategoryId() == productModelList.get(i).getCategoryCode())
                        productModelList.get(i).setCategoryName(goodsCategoryModelList.get(j).getCategoryName());
                }
            } else {
                productModelList.remove(i);
            }
        }

        for (int i = 0; i < productModelList.size(); i++) {
            if (productModelList.get(i).getAvalibilityCode() != 1 && productModelList.get(i).getAvalibilityCode() != 3)
                serialNumberList.add(productModelList.get(i).getSerialNumber());
        }
        for (int i = 0; i < byerModelList.size(); i++) {
            byerNameList.add(byerModelList.get(i).getSername() + " " + byerModelList.get(i).getName().trim().toUpperCase().charAt(0) + "." +
                    byerModelList.get(i).getPatronimic().trim().toUpperCase().charAt(0) + ".");
        }

        modelAndView.addObject("discountNameList", discountNameList);
        modelAndView.addObject("data", byerNameList);
        modelAndView.addObject("serialNumber", serialNumberList);
        return modelAndView;
    }

    @RequestMapping(value = "saleProduct")
    public ModelAndView saleProduct(@ModelAttribute(value = "saleData") SaleData model, HttpServletResponse response) {
        long time = System.currentTimeMillis();
        if (time - SallerManager.getInstance().getTime() < 20000) {
            ModelAndView modelAndView = new ModelAndView("error");
            modelAndView.addObject("message", new CallbackModel("с момента последней продажи прошло меньше двадцати секунд"));
            return modelAndView;
        } else {
            List<ProductModel> productModelList = productDao.productList1();
            if (productModelList.size() > 0) {
                ProductModel productModel = null;
                model.setSerialNumber(model.getSerialNumber().trim());
                for (int i = 0; i < productModelList.size(); i++) {
                    if (productModelList.get(i).getSerialNumber().equalsIgnoreCase(model.getSerialNumber())) {
                        productModel = productModelList.get(i);
                        break;
                    }
                }

                if(productModel == null){
                    ModelAndView g = new ModelAndView("error");
                    g.addObject("message",new CallbackModel("Неверно введенный серийный номер"));
                    return g;
                }
                if (productModel.getAvalibilityCode() == 1) {
                    ModelAndView modelAndView = new ModelAndView("error");
                    modelAndView.addObject("message", new CallbackModel("Данного товара нет на складе"));
                    return modelAndView;
                }

                if (productModel.getAvalibilityCode() == 3) {
                    ModelAndView modelAndView = new ModelAndView("error");
                    modelAndView.addObject("message", new CallbackModel("Данный товар ожидается"));
                    return modelAndView;
                }
                goodsCategoryDAO.getJdbcTemplate().update("UPDATE product SET availabilityCode = 3 WHERE serialNumber = ?", model.getSerialNumber());

                int byerId = 0;
                List<ByerModel> byerModelList = byerDAO.byerList();
                for (int i = 0; i < byerModelList.size(); i++) {
                    String sername = model.getByerName().split("\\.")[0].split(" ")[0];
                    if (byerModelList.get(i).getSername().equalsIgnoreCase(sername)
                            && model.getByerName().split("\\.")[0].split(" ")[1].equalsIgnoreCase(String.valueOf(byerModelList.get(i).getName().charAt(0)))
                            && model.getByerName().split("\\.")[1].equalsIgnoreCase(String.valueOf(byerModelList.get(i).getPatronimic().charAt(0))))
                        byerId = byerModelList.get(i).getByerId();
                }

                UpdateTablesDAO.saveOrUpdate(soldGoodsDAO.getJdbcTemplate()
                        , new Object[]{model.getId(), new Date(), byerId, productModel.getSerialNumber()}
                        , "INSERT INTO `salesContracts`(`salesId`, `date`, `byerId`, `goodsId`) VALUES (?,?,?,?)");


                ModelAndView modelAndView = new ModelAndView("success");
                modelAndView.addObject("message", new CallbackModel("Продажа произошла успешно"));
                SallerManager.getInstance().setTime(System.currentTimeMillis());
                generateCheck(productModel);
                try {

                    InputStream is = new FileInputStream("/home/stus/Downloads/SpringMvcJdbcTemplate (2)/src/main/webapp/resources/receipt.pdf");
                    // copy it to response's OutputStream
                    org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
                    response.flushBuffer();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    throw new RuntimeException("IOError writing file to output stream");
                }
                return modelAndView;
            } else {
                ModelAndView modelAndView = new ModelAndView("error");
                modelAndView.addObject("message",new CallbackModel("Список товаров пуст"));
                return modelAndView;
            }
        }
    }
    private void generateCheck(ProductModel productModel) {
        try {
            createTemplate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Receipt receipt = new Receipt(
                productModel.getSerialNumber(),
                Double.parseDouble(productModel.getPrice()),
                productModel.getName());

        try {
            fillInReceipt(receipt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createTemplate() throws Exception {
        Document document = new Document();

        Font font1 = new Font(Font.FontFamily.HELVETICA,
                32, Font.BOLD);
        Font font2 = new Font(Font.FontFamily.HELVETICA,
                16, Font.ITALIC | Font.UNDERLINE);

        PdfWriter.getInstance(document,
                new FileOutputStream("/home/stus/Downloads/SpringMvcJdbcTemplate (2)/src/main/webapp/resources/template.pdf"));

        document.open();

        // отцентрированный параграф
        Paragraph title = new Paragraph("Receipt", font1);
        title.setAlignment(Element.ALIGN_CENTER);
        title.setSpacingAfter(32);
        document.add(title);

        // параграф с текстом
        Paragraph serialNum = new Paragraph();
        serialNum.setFont(font2);
        serialNum.setSpacingAfter(8);
        serialNum.add(new Chunk("Serial Number"));
        document.add(serialNum);

        // параграф с добавленным чанком текста
        Paragraph amount = new Paragraph();
        amount.setFont(font2);
        amount.setSpacingAfter(8);
        amount.add(new Chunk("Amount"));
        document.add(amount);

        // параграф с фразой, в которую добавлен чанк
        Paragraph date = new Paragraph();
        date.setFont(font2);
        Phrase phrase = new Phrase();
        phrase.add(new Chunk("Date"));
        date.add(phrase);
        document.add(date);

        document.add(new Paragraph("Name", font2));




        document.close();
    }

    public void fillInReceipt(Receipt receipt)
            throws Exception {
        PdfReader reader = new PdfReader(
                new FileInputStream("/home/stus/Downloads/SpringMvcJdbcTemplate (2)/src/main/webapp/resources/template.pdf"));
        PdfStamper stamper = new PdfStamper(reader,
                new FileOutputStream("/home/stus/Downloads/SpringMvcJdbcTemplate (2)/src/main/webapp/resources/receipt.pdf"));

        PdfContentByte stream = stamper.getOverContent(1);
        stream.beginText();
        stream.setColorFill(BaseColor.BLUE);

        BaseFont font = BaseFont.createFont();

        float pageWidth = reader.getPageSize(1).getWidth();
        stream.setFontAndSize(font, 16);
        float v = stream.getEffectiveStringWidth(
                receipt.getPurpose(), false);

        float fitSize = (pageWidth - 40 * 2) * 16 / v;
        stream.setFontAndSize(font, 16);
        stream.setTextMatrix(pageWidth - v - 40, 710);
        stream.showText(receipt.getPurpose());

        stream.setFontAndSize(font, 16);

        String amount = NumberFormat.getCurrencyInstance()
                .format(receipt.getAmount());
        v = stream.getEffectiveStringWidth(amount, false);
        stream.setTextMatrix(pageWidth - v - 40, 685);
        stream.showText(amount);

        v = stream.getEffectiveStringWidth(
                receipt.getDate() + "", false);
        stream.setTextMatrix(pageWidth - v - 40, 660);
        stream.showText(receipt.getDate() + "");

        v = stream.getEffectiveStringWidth(
                receipt.getName(), false);
        stream.setTextMatrix(pageWidth - v - 40, 635);
        stream.showText(receipt.getName());

        stream.endText();
        stamper.setFullCompression();
        stamper.close();
    }

    static class Receipt {
        private String seraialNum;
        private String model;

        private String purpose;
        private double amount;
        private Date date = new Date();
        private String name;

        public Receipt(String seraialNum, double amount, String name) {
            this.purpose = seraialNum;
            this.amount = amount;
            this.name = name;
        }

        public String getPurpose() {
            return purpose;
        }

        public void setPurpose(String purpose) {
            this.purpose = purpose;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    @RequestMapping(value = "addByer")
    public ModelAndView addByer() {
        ModelAndView modelAndView = new ModelAndView("addByer");
        List<TelephoneTypeModel> telephoneTypeModelList = telephoneTypeDAO.telephoneTypeModelList();
        List<String> telephoneData = new ArrayList<>();
        for (int i = 0; i < telephoneTypeModelList.size(); i++) {
            telephoneData.add(telephoneTypeModelList.get(i).getName());
        }
        modelAndView.addObject("telephoneData", telephoneData);
        modelAndView.addObject("byerModel", new ByerModel());
        return modelAndView;
    }

    @RequestMapping(value = "addNewByer")
    public ModelAndView addNewByer(@ModelAttribute(value = "byerModel") ByerModel byerModel) {
        ModelAndView modelAndView = new ModelAndView();
        List<TelephoneTypeModel> telephoneTypeModelList = telephoneTypeDAO.telephoneTypeModelList();
        int telephoneTypeId = 0;
        for (int i = 0; i < telephoneTypeModelList.size(); i++) {
            if (telephoneTypeModelList.get(i).getName().equalsIgnoreCase(byerModel.getTelephoneTypeName()))
                telephoneTypeId = telephoneTypeModelList.get(i).getTelephoneTypeId();
        }
        if (byerModel.isValid()) {
            UpdateTablesDAO.saveOrUpdate(byerDAO.getJdbcTemplate()
                    , new Object[]{null, telephoneTypeId, byerModel.getEmail(), byerModel.getTelephone(), byerModel.getName(), byerModel.getSername(), byerModel.getPatronimic()}
                    , "INSERT INTO `byer`(`byerId`, `telephoneTypeId`, `email`, `telephone`, `name`, `sername`, `patromimic`) " +
                            "VALUES(?,?,?,?,?,?,?)");

            modelAndView = sale();
        } else {
            CallbackModel callbackModel = new CallbackModel("Неверно введены данные");
            modelAndView.setViewName("error");
            modelAndView.addObject("message", callbackModel);
        }
        return modelAndView;
    }
}
//todo рекваер поля
//fixme проверка на отрицательные поля
//fixme убрать проверку на серийный номер
//fixme оприходование товара
//fixme изменить удаление
