package net.codejava.spring.config;

import javax.sql.DataSource;

import net.codejava.spring.dao.*;

import net.codejava.spring.validators.DropDownValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages="net.codejava.spring")
@EnableWebMvc
public class MvcConfiguration extends WebMvcConfigurerAdapter{

	@Bean
	public ViewResolver getViewResolver(){
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/ds?useUnicode=true&characterEncoding=UTF-8");
		dataSource.setUsername("root");
		dataSource.setPassword("SwE-R7-+RUR");
		return dataSource;
	}

	
	@Bean
	public ContactDAO getContactDAO() {
		return new ContactDAOImpl(getDataSource());
	}

	@Bean
	public UserDAO getUserDAO(){
		return new UserDAO(getDataSource());
	}

	@Bean
	ProductDao getProductDao(){
		return new ProductDao(getDataSource());
	}

	@Bean
	StreetsTypeDAO getStreetsTypeDAO(){
		return new StreetsTypeDAO(getDataSource());
	}

	@Bean
	ByerDAO getByerDAO(){return  new ByerDAO(getDataSource());}

	@Bean
	AddressDao getAddressDao(){return new AddressDao(getDataSource());}

	@Bean
	CountryesDao getCountryesDao(){return new CountryesDao(getDataSource());}

	@Bean
	DeliveredGoodsDAO getDeliveredGoodsDAO(){return new DeliveredGoodsDAO(getDataSource());}


	@Bean
	GoodsCategoryDAO getGoodsCategoryDAO(){return new GoodsCategoryDAO(getDataSource());}

	@Bean
	IndividualSuppliersDAO getIndividualSuppliersDAO(){return new IndividualSuppliersDAO(getDataSource());}

	@Bean
	LegalByerDAO getLegalByerDAO(){return new LegalByerDAO(getDataSource());}

	@Bean
	LegalSuppliersDAO getLegalSuppliersDAO(){return new LegalSuppliersDAO(getDataSource());}

	@Bean
	LocalityDAO getLocalityDAO(){return new LocalityDAO(getDataSource());}

	@Bean
	LocalityTypeDAO getLocalityTypeDAO(){return new LocalityTypeDAO(getDataSource());}

	@Bean
	ManufacturerDAO getManufacturerDAO(){return new ManufacturerDAO(getDataSource());}

	@Bean
	RegionDAO getRegionDAO(){return new RegionDAO(getDataSource());}

	@Bean
	SalesContractDAO getSalesContractDAO(){return new SalesContractDAO(getDataSource());}

	@Bean
	SoldGoodsDAO getSoldGoodsDAO(){return new SoldGoodsDAO(getDataSource());}

	@Bean
	StockDAO getStockDAO(){return new StockDAO(getDataSource());}

	@Bean
	StreetsDAO getStreetsDAO(){return new StreetsDAO(getDataSource());}

	@Bean
	SuppliersDAO getSuppliersDAO(){return new SuppliersDAO(getDataSource());}

	@Bean
	SupplyContractDAO getSupplyContractDAO(){return new SupplyContractDAO(getDataSource());}

	@Bean
	TelephoneTypeDAO getTelephoneTypeDAO(){return new TelephoneTypeDAO(getDataSource());}

	@Bean
	DropDownValidator getDownValidator(){return new DropDownValidator();}

}
