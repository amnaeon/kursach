package net.codejava.spring.utils;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by stus on 02.05.17.
 */
public class StringUtils {
    public static final String SYMBOLS = "[]{}!@#$^&*()-_+=№\"';:%?.,/<>~";
    public static final String NUBMER = "1234567890";

    public static String getRandomStr() {
        return new BigInteger(68, new SecureRandom()).toString(32);
    }

    public static boolean isContainChar(String... s) {
        for (String t : s) {
            for (int j = 0; j < t.length(); j++) {
                for (int i = 0; i < SYMBOLS.length(); i++) {
                    if (t.charAt(j) == SYMBOLS.charAt(i)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static boolean emailValidator(String s) {
        for (int j = 0; j < s.length(); j++) {
            for (int i = 0; i < SYMBOLS.replaceAll("@", "").length(); i++) {
                if (s.charAt(j) == SYMBOLS.replaceAll("@", "").charAt(i)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean NumValidator(String s) {
        for (int i = 0; i < s.length(); i++) {
            boolean isValid = false;
            for (int j = 0; j < NUBMER.length(); j++) {
                if (s.charAt(i) == NUBMER.charAt(j))
                    isValid = true;
            }
            if (!isValid)
                return false;
        }
        return true;
    }

    public static boolean PhoneValidator(String s) {
        if (s.charAt(0) == '+')
            try {

                return NumValidator(s.substring(1, s.length() - 1));
            } catch (Exception e) {
                return false;
            }
        else return NumValidator(s);
    }
}
