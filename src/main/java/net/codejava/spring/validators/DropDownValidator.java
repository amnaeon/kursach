package net.codejava.spring.validators;

import net.codejava.spring.model.DropDownValuesModel;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by stus on 26.04.17.
 */
public class DropDownValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return DropDownValuesModel.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "please select item");
    }
}
