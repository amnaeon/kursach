package net.codejava.spring.consts;

/**
 * Created by stus on 11.04.17.
 */
public class Const {
    public class User{
        public static final String USER = "seller";
        public static final String ADMIN = "admin";
        public static final int GET_ALL_USER_TYPE = 0;
        public static final int GET_WORKER_TYPE = 1;

    }

    public class Views{
        public static final String PRODUCT_VIEW = "product";
        public static final String STREE_TYPE_VIEW = "streetType";
    }
}
