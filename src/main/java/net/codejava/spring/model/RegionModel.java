package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class RegionModel {
    private Integer regionId;
    private String name;
    private Integer countryId;
    private String countryName;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public boolean isValid() {
        if (this.name.isEmpty()
                || !StringUtils.isContainChar(this.name)
                || this.name.length() > 20
                || StringUtils.NumValidator(this.name))
            return false;
        else return true;
    }

    public static List<String> getNameList(List<RegionModel> list) {
        List<String> res = new ArrayList<>();
        for (RegionModel aList : list) {
            res.add(aList.name);
        }
        return res;
    }
}
