package net.codejava.spring.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 11.04.17.
 */
public class StreetTypeModel {
    private Integer streetTypeId;
    private String name;

    public Integer getStreetTypeId() {
        return streetTypeId;
    }

    public void setStreetTypeId(Integer streetTypeId) {
        this.streetTypeId = streetTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<String> getNameList(List<StreetTypeModel> list) {
        List<String> res = new ArrayList<>();
        for (StreetTypeModel aList : list) {
            res.add(aList.name);
        }
        return res;
    }
}
