package net.codejava.spring.model;

public class NewSupplierInfoModel {
    private Integer code;
    private String country = "";
    private String region = "";
    private String locality = "";
    private String street = "";
    private String telephoneCode = "";
    private String postCode = "";
    private boolean isEntity;
    private String telephoneNumber = "";
    private String buildNum = "";
    private LegalSuppliersModel legalSuppliersModel;
    private IndividualSuppliersModel individualSuppliersModel;

    public String getBuildNum() {
        return buildNum;
    }

    public void setBuildNum(String buildNum) {
        this.buildNum = buildNum;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public boolean isEntity() {
        return isEntity;
    }

    public void setEntity(boolean entity) {
        isEntity = entity;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTelephoneCode() {
        return telephoneCode;
    }

    public void setTelephoneCode(String telephoneCode) {
        this.telephoneCode = telephoneCode;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public LegalSuppliersModel getLegalSuppliersModel() {
        return legalSuppliersModel;
    }

    public void setLegalSuppliersModel(LegalSuppliersModel legalSuppliersModel) {
        this.legalSuppliersModel = legalSuppliersModel;
    }

    public IndividualSuppliersModel getIndividualSuppliersModel() {
        return individualSuppliersModel;
    }

    public void setIndividualSuppliersModel(IndividualSuppliersModel individualSuppliersModel) {
        this.individualSuppliersModel = individualSuppliersModel;
    }
}
