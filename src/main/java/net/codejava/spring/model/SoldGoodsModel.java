package net.codejava.spring.model;

public class SoldGoodsModel {
    private Integer sellingGoodsId;
    private Integer discountId;
    private Integer count;
    private Integer currenccyId;

    public Integer getSellingGoodsId() {
        return sellingGoodsId;
    }

    public void setSellingGoodsId(Integer sellingGoodsId) {
        this.sellingGoodsId = sellingGoodsId;
    }

    public Integer getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getCurrenccyId() {
        return currenccyId;
    }

    public void setCurrenccyId(Integer currenccyId) {
        this.currenccyId = currenccyId;
    }
}
