package net.codejava.spring.model;

import net.codejava.spring.dao.IndividualSuppliersDAO;
import net.codejava.spring.dao.LegalSuppliersDAO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by stus on 11.04.17.
 */
public class SuppliersModel {

    private Integer suppliersId;
    private Integer addressId;
    private Integer telephoneTypeId;
    private String phone;
    private String name;
    private String buildNum;

    public SuppliersModel(Integer suppliersId, Integer addressId, Integer telephoneTypeId, String phone, String name) {
        this.suppliersId = suppliersId;
        this.addressId = addressId;
        this.telephoneTypeId = telephoneTypeId;
        this.phone = phone;
        this.name = name;
    }

    public SuppliersModel() {
    }

    public String getBuildNum() {
        return buildNum;
    }

    public void setBuildNum(String buildNum) {
        this.buildNum = buildNum;
    }

    public Integer getSuppliersId() {
        return suppliersId;
    }

    public void setSuppliersId(Integer suppliersId) {
        this.suppliersId = suppliersId;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public Integer getTelephoneTypeId() {
        return telephoneTypeId;
    }

    public void setTelephoneTypeId(Integer telephoneTypeId) {
        this.telephoneTypeId = telephoneTypeId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<String> getNameList(List<SuppliersModel> list, List<IndividualSuppliersModel> individualSuppliersModelList,
                                           List<LegalSuppliersModel> legalSuppliersModelList) {
        List<String> res = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < individualSuppliersModelList.size(); j++) {
                if (Objects.equals(list.get(i).getSuppliersId(), individualSuppliersModelList.get(j).getSupplierId())) {
                    IndividualSuppliersModel individualSuppliersModel = individualSuppliersModelList.get(j);
                    list.get(i).setName(individualSuppliersModel.getSurname() + " " + String.valueOf(individualSuppliersModel.getName().charAt(0)).toUpperCase()
                            + "." + String.valueOf(individualSuppliersModel.getPatronymic().charAt(0)).toUpperCase() + ".");
                    res.add(list.get(i).getName());
                    break;
                }

            }
            for (int j = 0; j < legalSuppliersModelList.size(); j++) {
                if (Objects.equals(list.get(i).getSuppliersId(), legalSuppliersModelList.get(j).getSuppliersId())) {
                    list.get(i).setName(legalSuppliersModelList.get(j).getName());
                    res.add(list.get(i).getName());
                    break;
                }
            }
        }

        return res;
    }

    public static List<SuppliersModel> setName(List<SuppliersModel> list, List<IndividualSuppliersModel> individualSuppliersModelList,
                                               List<LegalSuppliersModel> legalSuppliersModelList){
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < individualSuppliersModelList.size(); j++) {
                if (Objects.equals(list.get(i).getSuppliersId(), individualSuppliersModelList.get(j).getSupplierId())) {
                    IndividualSuppliersModel individualSuppliersModel = individualSuppliersModelList.get(j);
                    list.get(i).setName(individualSuppliersModel.getSurname() + " " + String.valueOf(individualSuppliersModel.getName().charAt(0)).toUpperCase()
                            + "." + String.valueOf(individualSuppliersModel.getPatronymic().charAt(0)).toUpperCase() + ".");
                    break;
                }

            }
            for (int j = 0; j < legalSuppliersModelList.size(); j++) {
                if (Objects.equals(list.get(i).getSuppliersId(), legalSuppliersModelList.get(j).getSuppliersId())) {
                    list.get(i).setName(legalSuppliersModelList.get(j).getName());
                    break;
                }
            }
        }
    return list;
    }
}
