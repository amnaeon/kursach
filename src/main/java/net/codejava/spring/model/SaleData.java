package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

/**
 * Created by stus on 08.05.17.
 */
public class SaleData {
    private String serialNumber = "";
    private String count = "";
    private String id = "";
    private int byerId = 0;
    private String byerName = "";
    private String discount = "";

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getByerName() {
        return byerName;
    }

    public void setByerName(String byerName) {
        this.byerName = byerName;
    }

    public SaleData() {
        id = StringUtils.getRandomStr();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getCount() {
        return count;
    }

    public int getByerId() {
        return byerId;
    }

    public void setByerId(int byerId) {
        this.byerId = byerId;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
