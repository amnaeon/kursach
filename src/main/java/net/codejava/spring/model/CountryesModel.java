package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CountryesModel {
    private Integer countryId;
    private String name;
    private String telephoneCode;

    public boolean isValid() {
        if (this.telephoneCode.isEmpty()
                || this.name.isEmpty()
                || !StringUtils.isContainChar(this.name)
                || this.name.length() > 20 || this.telephoneCode.length() > 20
                || !StringUtils.PhoneValidator(telephoneCode))
            return false;
        else return true;
    }
    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getTelephoneCode() {
        return telephoneCode;
    }

    public void setTelephoneCode(String telephoneCode) {
        this.telephoneCode = telephoneCode;
    }

    public static List<String> getNameList(List<CountryesModel> list) {
        List<String> res = new ArrayList<>();
        for (CountryesModel aList : list) {
            res.add(aList.name);
        }
        return res;
    }
}
