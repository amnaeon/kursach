package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

import java.util.Date;

public class SupplyContractModel {
    private String supplyContractId = StringUtils.getRandomStr();
    private Date couclusionDate;
    private Date expirationDate;
    private Date deliveryDate;
    private int suppliersId;


    public SupplyContractModel(String supplyContractId, Date couclusionDate, Date expirationDate, Date deliveryDate, int suppliersId) {
        this.supplyContractId = supplyContractId;
        this.couclusionDate = couclusionDate;
        this.expirationDate = expirationDate;
        this.deliveryDate = deliveryDate;
        this.suppliersId = suppliersId;
    }

    public SupplyContractModel() {
    }

    public boolean isValid() {
        if (this.couclusionDate == null || this.expirationDate == null || this.deliveryDate == null || this.couclusionDate.after(this.expirationDate)
                || this.couclusionDate.after(this.deliveryDate) || this.deliveryDate.after(this.expirationDate))
            return false;
        else return true;
    }

    public int getSuppliersId() {
        return suppliersId;
    }

    public void setSuppliersId(int suppliersId) {
        this.suppliersId = suppliersId;
    }

    public String getSupplyContractId() {
        return supplyContractId;
    }

    public void setSupplyContractId(String supplyContractId) {
        this.supplyContractId = supplyContractId;
    }

    public Date getCouclusionDate() {
        return couclusionDate;
    }

    public void setCouclusionDate(Date couclusionDate) {
        this.couclusionDate = couclusionDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
}
