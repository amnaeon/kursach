package net.codejava.spring.model;

/**
 * Created by stus on 11.04.17.
 */
public class DiscountsModel {
    private Integer discountId;
    private String discountName;
    private Integer discountPersent;

    public DiscountsModel() {
    }

    public DiscountsModel(Integer discountId, String discountName, Integer discountPersent) {
        this.discountId = discountId;
        this.discountName = discountName;
        this.discountPersent = discountPersent;
    }

    public Integer getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public Integer getDiscountPersent() {
        return discountPersent;
    }

    public void setDiscountPersent(Integer discountPersent) {
        this.discountPersent = discountPersent;
    }
}
