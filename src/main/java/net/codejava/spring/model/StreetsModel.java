package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

/**
 * Created by stus on 11.04.17.
 */
public class StreetsModel {
    private Integer streetId;
    private String name;
    private Integer streetTypeId;
    private Integer locaityTypeId;
    private String streetTypeName;
    private String localityTypeName;

    public boolean isValid() {
        if (this.name.isEmpty()
                || !StringUtils.isContainChar(this.name)
                || this.name.length() > 20)
            return false;
        else return true;
    }

    public String getStreetTypeName() {
        return streetTypeName;
    }

    public void setStreetTypeName(String streetTypeName) {
        this.streetTypeName = streetTypeName;
    }

    public String getLocalityTypeName() {
        return localityTypeName;
    }

    public void setLocalityTypeName(String localityTypeName) {
        this.localityTypeName = localityTypeName;
    }

    public Integer getStreetId() {
        return streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStreetTypeId() {
        return streetTypeId;
    }

    public void setStreetTypeId(Integer streetTypeId) {
        this.streetTypeId = streetTypeId;
    }

    public Integer getLocaityTypeId() {
        return locaityTypeId;
    }

    public void setLocaityTypeId(Integer locaityTypeId) {
        this.locaityTypeId = locaityTypeId;
    }
}
