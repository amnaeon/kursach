package net.codejava.spring.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 11.04.17.
 */
public class LocalityTypeModel {
    private Integer localityTypeId;
    private String localityName;

    public Integer getLocalityTypeId() {
        return localityTypeId;
    }

    public void setLocalityTypeId(Integer localityTypeId) {
        this.localityTypeId = localityTypeId;
    }

    public String getLocalityName() {
        return localityName;
    }

    public void setLocalityName(String localityName) {
        this.localityName = localityName;
    }

    public static List<String> getNameList(List<LocalityTypeModel> list) {
        List<String> res = new ArrayList<>();
        for (LocalityTypeModel aList : list) {
            res.add(aList.localityName);
        }
        return res;
    }
}
