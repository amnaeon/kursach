package net.codejava.spring.model;

import java.util.Date;

/**
 * Created by stus on 11.04.17.
 */
public class DeliveredGoodsModel {
    private String supplyContractId;
    private String count;
    private String name;
    private String model;
    private Integer categoryCode;
    private Integer manufacturerId;
    private String price;
    private String avalibilityName;
    private String categoryName;
    private String manufacturerName;


    public String getSupplyContractId() {
        return supplyContractId;
    }

    public void setSupplyContractId(String supplyContractId) {
        this.supplyContractId = supplyContractId;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(Integer categoryCode) {
        this.categoryCode = categoryCode;
    }

    public Integer getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Integer manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAvalibilityName() {
        return avalibilityName;
    }

    public void setAvalibilityName(String avalibilityName) {
        this.avalibilityName = avalibilityName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
}
