package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

/**
 * Created by stus on 11.04.17.
 */
public class LegalSuppliersModel {
    private Integer suppliersId = 0;
    private String name = "";
    private String taxNumer = "";
    private String certificateNumber = "";

    public Integer getSuppliersId() {
        return suppliersId;
    }

    public void setSuppliersId(Integer suppliersId) {
        this.suppliersId = suppliersId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxNumer() {
        return taxNumer;
    }

    public void setTaxNumer(String taxNumer) {
        this.taxNumer = taxNumer;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public boolean isValid() {
        if (this.name.isEmpty() || this.taxNumer.isEmpty() || this.certificateNumber.isEmpty()
                || !StringUtils.isContainChar(this.taxNumer, this.certificateNumber, this.name)
                || this.certificateNumber.length() > 20
                || this.name.length() > 20
                || this.taxNumer.length() > 20)
            return false;
        else return true;
    }
}
