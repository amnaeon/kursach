package net.codejava.spring.model;

public class StockModel {
    private Integer availabilityCode;
    private String availabilityStatus;

    public StockModel(Integer availabilityCode, String availabilityStatus) {
        this.availabilityCode = availabilityCode;
        this.availabilityStatus = availabilityStatus;
    }

    public StockModel() {
    }

    public Integer getAvailabilityCode() {
        return availabilityCode;
    }

    public void setAvailabilityCode(Integer availabilityCode) {
        this.availabilityCode = availabilityCode;
    }

    public String getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(String availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }
}
