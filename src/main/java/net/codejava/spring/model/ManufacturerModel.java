package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

public class ManufacturerModel {
    private Integer manufacturerId;
    private String manufacturerName;
    private Integer countryId;
    private Integer address;
    private String contactPerson;
    private String telephone;
    private String country;
    private String region;
    private String locality;
    private String street;
    private String buildNum;
    private String telephoneNumber;
    private String postCode;

    public ManufacturerModel(Integer manufacturerId, String manufacturerName, Integer countryId, Integer address, String contactPerson, String telephone) {
        this.manufacturerId = manufacturerId;
        this.manufacturerName = manufacturerName;
        this.countryId = countryId;
        this.address = address;
        this.contactPerson = contactPerson;
        this.telephone = telephone;
    }

    public boolean isValid() {
        if (this.manufacturerName.isEmpty() || this.contactPerson.isEmpty()
                || this.buildNum.isEmpty()
                || this.telephoneNumber.isEmpty()
                || this.postCode.isEmpty()
                || !StringUtils.isContainChar(manufacturerName, contactPerson, buildNum)
                || !StringUtils.NumValidator(telephoneNumber)
                || !StringUtils.NumValidator(postCode)
                || manufacturerName.length() > 20
                || contactPerson.length() > 20
                || buildNum.length() > 20
                || manufacturerName.length() > 20
                || postCode.length() > 20)
            return false;
        else {
            return true;
        }
    }


    public ManufacturerModel() {
    }

    public Integer getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Integer manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getAddress() {
        return address;
    }

    public void setAddress(Integer address) {
        this.address = address;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getLocality() {
        return locality;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return street;
    }

    public void setBuildNum(String buildNum) {
        this.buildNum = buildNum;
    }

    public String getBuildNum() {
        return buildNum;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPostCode() {
        return postCode;
    }
}
