package net.codejava.spring.model;

import java.util.Date;
import java.util.List;

/**
 * Created by stus on 30.05.17.
 */
public class Last {
    private List<ProductModel> productModels;
    private String serialNum = "";
    private String name =  "";
    private Date date = new Date();

    public Last(List<ProductModel> productModels) {
        this.productModels = productModels;
    }

    public Last() {
    }

    public List<ProductModel> getProductModels() {
        return productModels;
    }

    public void setProductModels(List<ProductModel> productModels) {
        this.productModels = productModels;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
