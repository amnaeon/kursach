package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

import java.util.Date;

/**
 * Created by stus on 04.05.17.
 */
public class AddProductModel {
    private String categoryName = "";
    private String manufacturerName = "";
    private String avalibilityName = "";
    private String serialNumber = "";
    private String name = "";
    private String model = "";
    private Date releaseDate;
    private Integer avalibilityCode = 0;
    private Integer categoryCode = 0;
    private Integer manufacturerId = 0;
    private String price = "";
    private String count;
    public boolean isValid(){
        if(this.name.isEmpty()||this.model.isEmpty()||this.count==null||this.categoryName.isEmpty()
                ||this.manufacturerName.isEmpty()
                ||!StringUtils.isContainChar(name,model,categoryName,manufacturerName)
                ||name.length()>20
                ||model.length()>20
                ||categoryName.length()>20
                ||manufacturerName.length()>20)
            return false;
        else {
            try {
                Float.parseFloat(price);
                Float.parseFloat(count);
                if(Float.parseFloat(price)<=0||Float.parseFloat(count)<=0){
                    return false;
                }
            }catch (Exception e){
                return false;
            }
            return true;
        }
    }
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getAvalibilityName() {
        return avalibilityName;
    }

    public void setAvalibilityName(String avalibilityName) {
        this.avalibilityName = avalibilityName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getAvalibilityCode() {
        return avalibilityCode;
    }

    public void setAvalibilityCode(Integer avalibilityCode) {
        this.avalibilityCode = avalibilityCode;
    }

    public Integer getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(Integer categoryCode) {
        this.categoryCode = categoryCode;
    }

    public Integer getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Integer manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getСount() {
        return count;
    }

    public void setСount(String сount) {
        this.count = сount;
    }
}
