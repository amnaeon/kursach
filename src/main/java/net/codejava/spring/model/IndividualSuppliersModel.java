package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 11.04.17.
 */
public class IndividualSuppliersModel {
    private Integer supplierId = 0;
    private String documentId = "";
    private String  name = "";
    private String surname = "";
    private String patronymic = "";

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public static List<String> getNameList(List<IndividualSuppliersModel> list) {
        List<String> res = new ArrayList<>();
        for (IndividualSuppliersModel aList : list) {
            res.add(aList.surname+aList.name.substring(0,1).toUpperCase()+"."
                    +aList.patronymic.substring(0,1).toUpperCase()+".");
        }
        return res;
    }

    public boolean isValid(){
        if(this.name.isEmpty()||this.documentId.isEmpty()
                ||this.surname.isEmpty()
                ||this.patronymic.isEmpty()
                ||!StringUtils.isContainChar(this.documentId,this.surname,this.patronymic,this.name)
                ||this.surname.length()>20
                ||this.name.length()>20
                ||this.patronymic.length()>20)
            return false;
        else return true;
    }
}
