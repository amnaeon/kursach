package net.codejava.spring.model;

/**
 * Created by stus on 08.05.17.
 */
public class CallbackModel {
    private String message = "";

    public CallbackModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
