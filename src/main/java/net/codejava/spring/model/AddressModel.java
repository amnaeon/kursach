package net.codejava.spring.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 11.04.17.
 */
public class AddressModel {
    private Integer addressId;
    private String postcode;
    private Integer streetId;
    private String buildingAddress;

    public String getBuildingAddress() {
        return buildingAddress;
    }

    public void setBuildingAddress(String buildingAddress) {
        this.buildingAddress = buildingAddress;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Integer getStreetId() {
        return streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }


}
