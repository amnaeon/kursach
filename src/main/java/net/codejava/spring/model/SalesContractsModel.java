package net.codejava.spring.model;

/**
 * Created by stus on 11.04.17.
 */
public class SalesContractsModel {
    private Integer salesId;
    private String date;
    private Integer byerId;

    public SalesContractsModel(Integer salesId, String date, Integer byerId) {
        this.salesId = salesId;
        this.date = date;
        this.byerId = byerId;
    }

    public SalesContractsModel() {
    }

    public Integer getSalesId() {
        return salesId;
    }

    public void setSalesId(Integer salesId) {
        this.salesId = salesId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getByerId() {
        return byerId;
    }

    public void setByerId(Integer byerId) {
        this.byerId = byerId;
    }
}
