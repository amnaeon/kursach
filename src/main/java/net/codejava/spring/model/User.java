package net.codejava.spring.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by stus on 11.04.17.
 */
public class User {
    @NotNull
    @Size(min = 4)
    private String login;
    @NotNull
    @Size(min = 4)
    private String password;
    private String type = "";
    private int id;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getType() {
        return type;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTypeFomList(List<User> userList) {
        for (int i = 0; i < userList.size(); i++) {
            if(this.login.equals(userList.get(i).getLogin())&&this.password.equals(userList.get(i).getPassword())) {
                type = userList.get(i).type;
                break;
            }
        }
    }
}
