package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

/**
 * Created by stus on 11.04.17.
 */
public class LocalityModel {
    private Integer localityId;
    private String telephoneCode;
    private String name;
    private Integer regionId;
    private Integer localityTypeId;
    private String regionName;
    private String localityTypeName;

    public boolean isValid() {
        if (this.telephoneCode.isEmpty()
                || this.name.isEmpty()
                || !StringUtils.isContainChar(this.name)
                || this.name.length() > 20 || this.telephoneCode.length() > 20
                || !StringUtils.PhoneValidator(telephoneCode)
                || StringUtils.NumValidator(this.name))
            return false;
        else return true;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getLocalityTypeName() {
        return localityTypeName;
    }

    public void setLocalityTypeName(String localityTypeName) {
        this.localityTypeName = localityTypeName;
    }

    public Integer getLocalityId() {
        return localityId;
    }

    public void setLocalityId(Integer localityId) {
        this.localityId = localityId;
    }

    public String getTelephoneCode() {
        return telephoneCode;
    }

    public void setTelephoneCode(String telephoneCode) {
        this.telephoneCode = telephoneCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public Integer getLocalityTypeId() {
        return localityTypeId;
    }

    public void setLocalityTypeId(Integer localityTypeId) {
        this.localityTypeId = localityTypeId;
    }
}
