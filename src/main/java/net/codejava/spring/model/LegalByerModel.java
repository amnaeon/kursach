package net.codejava.spring.model;

/**
 * Created by stus on 11.04.17.
 */
public class LegalByerModel {
    private Integer byerId;
    private String name;
    private Integer certificateNumber;
    private Integer taxNumber;

    public Integer getByerId() {
        return byerId;
    }

    public void setByerId(Integer byerId) {
        this.byerId = byerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(Integer certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public Integer getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(Integer taxNumber) {
        this.taxNumber = taxNumber;
    }
}
