package net.codejava.spring.model;

/**
 * Created by stus on 11.04.17.
 */
public class TelephoneTypeModel {
    private Integer telephoneTypeId;
    private String name;

    public Integer getTelephoneTypeId() {
        return telephoneTypeId;
    }

    public void setTelephoneTypeId(Integer telephoneTypeId) {
        this.telephoneTypeId = telephoneTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
