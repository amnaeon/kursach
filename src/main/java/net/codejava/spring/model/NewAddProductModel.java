package net.codejava.spring.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 29.05.17.
 */
public class NewAddProductModel {
    private List<AddProductModel> addProductModelList = new ArrayList<>();
    private String name ="";
    private boolean isSelected = false;


    public NewAddProductModel(List<AddProductModel> addProductModelList, String name) {
        this.addProductModelList = addProductModelList;
        this.name = name;
    }

    public NewAddProductModel() {
    }

    public List<AddProductModel> getAddProductModelList() {
        return addProductModelList;
    }

    public void setAddProductModelList(List<AddProductModel> addProductModelList) {
        this.addProductModelList = addProductModelList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
