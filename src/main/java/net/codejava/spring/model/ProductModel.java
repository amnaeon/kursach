package net.codejava.spring.model;

import java.util.Date;

public class ProductModel {
    private String serialNumber;
    private String name;
    private String model;
    private Date releaseDate;
    private Integer avalibilityCode;
    private Integer categoryCode;
    private Integer manufacturerId;
    private String price;
    private String  сount;
    private String avalibilityName;
    private String categoryName;
    private String manufacturerName;

    public String getAvalibilityName() {
        return avalibilityName;
    }

    public void setAvalibilityName(String avalibilityName) {
        this.avalibilityName = avalibilityName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String   getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getAvalibilityCode() {
        return avalibilityCode;
    }

    public void setAvalibilityCode(int avalibilityCode) {
        this.avalibilityCode = avalibilityCode;
    }

    public int getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(int categoryCode) {
        this.categoryCode = categoryCode;
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getСount() {
        return сount;
    }

    public void setСount(String сount) {
        this.сount = сount;
    }
}
