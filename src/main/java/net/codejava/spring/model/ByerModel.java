package net.codejava.spring.model;

import net.codejava.spring.utils.StringUtils;

public class ByerModel {
    private Integer byerId;
    private Integer telephoneTypeId;
    private String email;
    private String telephone;
    private String name;
    private String sername;
    private String patronimic;
    private String telephoneTypeName;

    public String getTelephoneTypeName() {
        return telephoneTypeName;
    }

    public void setTelephoneTypeName(String telephoneTypeName) {
        this.telephoneTypeName = telephoneTypeName;
    }

    public boolean isValid() {
        if (this.telephoneTypeName.isEmpty() || this.email.isEmpty() || this.telephone.isEmpty()
                || this.name.isEmpty() || this.sername.isEmpty() || this.patronimic.isEmpty()
                || !StringUtils.isContainChar(this.sername, this.name, this.patronimic)
                || !StringUtils.emailValidator(this.email)
                || this.email.length() > 20 || this.telephone.length() > 20 || this.email.length() > 20 ||
                this.name.length() > 20 || this.sername.length() > 20 || this.patronimic.length() > 20
                || !StringUtils.PhoneValidator(telephone))
            return false;
        else return true;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSername() {
        return sername;
    }

    public void setSername(String sername) {
        this.sername = sername;
    }

    public String getPatronimic() {
        return patronimic;
    }

    public void setPatronimic(String patronimic) {
        this.patronimic = patronimic;
    }

    public Integer getByerId() {
        return byerId;
    }

    public void setByerId(Integer byerId) {
        this.byerId = byerId;
    }

    public Integer getTelephoneTypeId() {
        return telephoneTypeId;
    }

    public void setTelephoneTypeId(Integer telephoneTypeId) {
        this.telephoneTypeId = telephoneTypeId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
