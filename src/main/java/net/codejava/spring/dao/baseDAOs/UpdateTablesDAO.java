package net.codejava.spring.dao.baseDAOs;

import net.codejava.spring.model.Contact;
import org.springframework.jdbc.core.JdbcTemplate;

public class UpdateTablesDAO {
    public static void saveOrUpdate(JdbcTemplate jdbcTemplate, Object[] params, String sql) {
        jdbcTemplate.update(sql, params);
    }

    public static void delete(JdbcTemplate jdbcTemplate, Object[] params, String sql) {
        jdbcTemplate.update(sql, params);
    }

}
