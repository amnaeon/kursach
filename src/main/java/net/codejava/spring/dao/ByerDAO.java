package net.codejava.spring.dao;

import net.codejava.spring.model.ByerModel;
import net.codejava.spring.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 12.04.17.
 */
public class ByerDAO {
    private JdbcTemplate jdbcTemplate;

    public ByerDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<ByerModel> byerList() {
        String sql = "SELECT * FROM byer";
        List<ByerModel> byerModelList = jdbcTemplate.query(sql, new RowMapper<ByerModel>() {

            @Override
            public ByerModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                ByerModel byer = new ByerModel();
                byer.setByerId(rs.getInt("byerId"));
                byer.setEmail(rs.getString("email"));
                byer.setTelephoneTypeId(rs.getInt("telephoneTypeId"));
                byer.setTelephone(rs.getString("telephone"));
                byer.setName(rs.getString("name"));
                byer.setSername(rs.getString("sername"));
                byer.setPatronimic(rs.getString("patromimic"));
                return byer;
            }

        });

        return byerModelList;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
