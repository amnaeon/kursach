package net.codejava.spring.dao;

import net.codejava.spring.model.SalesContractsModel;
import net.codejava.spring.model.SoldGoodsModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class SoldGoodsDAO {

    private JdbcTemplate jdbcTemplate;

    public SoldGoodsDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public List<SoldGoodsModel> soldGoodsModels() {
        String sql = "SELECT * FROM soldGoods";
        List<SoldGoodsModel> soldGoodsModels = jdbcTemplate.query(sql, new RowMapper<SoldGoodsModel>() {

            @Override
            public SoldGoodsModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                SoldGoodsModel contractsModel = new SoldGoodsModel();
                contractsModel.setSellingGoodsId(rs.getInt("sellingGoodsId"));
                contractsModel.setDiscountId(rs.getInt("discountId"));
                contractsModel.setCurrenccyId(rs.getInt("currencyId"));
                return contractsModel;
            }

        });

        return soldGoodsModels;
    }
}
