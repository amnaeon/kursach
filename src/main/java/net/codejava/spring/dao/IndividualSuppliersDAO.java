package net.codejava.spring.dao;

import net.codejava.spring.model.GoodsCategoryModel;
import net.codejava.spring.model.IndividualSuppliersModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class IndividualSuppliersDAO {
    private JdbcTemplate jdbcTemplate;

    public IndividualSuppliersDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<IndividualSuppliersModel> individualSuppliersModels() {
        String sql = "SELECT * FROM individualSuppliers";
        List<IndividualSuppliersModel> supliersList = jdbcTemplate.query(sql, new RowMapper<IndividualSuppliersModel>() {

            @Override
            public IndividualSuppliersModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                IndividualSuppliersModel suppliers = new IndividualSuppliersModel();
                suppliers.setSupplierId(rs.getInt("suppliersId"));
                suppliers.setDocumentId(rs.getString("documentId"));
                suppliers.setName(rs.getString("name"));
                suppliers.setSurname(rs.getString("surname"));
                suppliers.setPatronymic(rs.getString("patronymic"));
                return suppliers;
            }

        });

        return supliersList;
    }
}
