package net.codejava.spring.dao;

import net.codejava.spring.model.SuppliersModel;
import net.codejava.spring.model.SupplyContractModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class SupplyContractDAO {
    private JdbcTemplate jdbcTemplate;

    public SupplyContractDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<SupplyContractModel> supplyContractModelList() {
        String sql = "SELECT * FROM supplyСontracts";
        List<SupplyContractModel> suppliersModelList = jdbcTemplate.query(sql, new RowMapper<SupplyContractModel>() {

            @Override
            public SupplyContractModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                SupplyContractModel suppliersModel = new SupplyContractModel();
                suppliersModel.setCouclusionDate(rs.getDate("conclusionDate"));
                suppliersModel.setDeliveryDate(rs.getDate("deliveryDate"));
                suppliersModel.setExpirationDate(rs.getDate("expirationDate"));
                suppliersModel.setSupplyContractId(rs.getString("supplyContranctId"));
                suppliersModel.setSuppliersId(rs.getInt("suppliersId"));
                return suppliersModel;
            }

        });

        return suppliersModelList;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
