package net.codejava.spring.dao;

import net.codejava.spring.model.IndividualSuppliersModel;
import net.codejava.spring.model.LegalByerModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class LegalByerDAO {
    private JdbcTemplate jdbcTemplate;

    public LegalByerDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<LegalByerModel> legalByerModels() {
        String sql = "SELECT * FROM legalByer";
        List<LegalByerModel> supliersList = jdbcTemplate.query(sql, new RowMapper<LegalByerModel>() {

            @Override
            public LegalByerModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                LegalByerModel byer = new LegalByerModel();
                byer.setByerId(rs.getInt("byerId"));
                byer.setCertificateNumber(rs.getInt("certificateNumber"));
                byer.setName(rs.getString("name"));
                byer.setTaxNumber(rs.getInt("taxNumber"));
                return byer;
            }

        });

        return supliersList;
    }
}
