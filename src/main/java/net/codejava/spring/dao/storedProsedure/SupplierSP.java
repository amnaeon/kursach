package net.codejava.spring.dao.storedProsedure;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by stus on 01.05.17.
 */
public class SupplierSP {
    public static void make(int streetId,String postCode,int telephoneTypeId,String telephoneNumber,boolean isIndividualSupplier,
                     String documentId,String name, String surname,String patronymic,String legalName,String taxNumber,String certificateNumber) {
        Connection connection = null;
        CallableStatement callableStatement = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException cnfex) {
            System.out.println("Problem in loading MySQL JDBC driver");
            cnfex.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ds?useUnicode=true&characterEncoding=UTF-8", "root", "SwE-R7-+RUR");

            callableStatement = connection
                    .prepareCall("{call addSupplier(?,?,?,?,?,?,?,?,?,?,?,?)}");

            callableStatement.setInt(1, streetId);
            callableStatement.setString(2, postCode);
            callableStatement.setInt(3, telephoneTypeId);
            callableStatement.setString(4, telephoneNumber);
            callableStatement.setBoolean(5, isIndividualSupplier);
            callableStatement.setString(6, documentId);
            callableStatement.setString(7, name);
            callableStatement.setString(8, surname);
            callableStatement.setString(9, patronymic);
            callableStatement.setString(10, legalName);
            callableStatement.setString(11, taxNumber);
            callableStatement.setString(12, certificateNumber);
            callableStatement.executeUpdate();

        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        } finally {

            try {
                if (null != connection) {
                    callableStatement.close();
                    connection.close();
                }
            } catch (SQLException sqlex) {
                sqlex.printStackTrace();
            }
        }
    }
}


