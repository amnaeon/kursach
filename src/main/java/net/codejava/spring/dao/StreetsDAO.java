package net.codejava.spring.dao;

import net.codejava.spring.model.StockModel;
import net.codejava.spring.model.StreetsModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class StreetsDAO {
    private JdbcTemplate jdbcTemplate;

    public StreetsDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public List<StreetsModel> streetsModelList() {
        String sql = "SELECT * FROM streets";
        List<StreetsModel> streetsModelList = jdbcTemplate.query(sql, new RowMapper<StreetsModel>() {

            @Override
            public StreetsModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                StreetsModel streetsModel = new StreetsModel();
                streetsModel.setLocaityTypeId(rs.getInt("localityId"));
                streetsModel.setName(rs.getString("name"));
                streetsModel.setStreetId(rs.getInt("streetId"));
                streetsModel.setStreetTypeId(rs.getInt("streetTypeId"));
                return streetsModel;
            }

        });

        return streetsModelList;
    }
}
