package net.codejava.spring.dao;

import net.codejava.spring.model.AddressModel;
import net.codejava.spring.model.CountryesModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 12.04.17.
 */
public class CountryesDao {
    private JdbcTemplate jdbcTemplate;

    public CountryesDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<CountryesModel> addressList() {
        String sql = "SELECT * FROM countries";
        List<CountryesModel> countryesModelList = jdbcTemplate.query(sql, new RowMapper<CountryesModel>() {

            @Override
            public CountryesModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                CountryesModel countryesModel = new CountryesModel();
                countryesModel.setCountryId(rs.getInt("countryId"));
                countryesModel.setName(rs.getString("name"));
                countryesModel.setTelephoneCode(rs.getString("telephoneCode"));
                return countryesModel;
            }

        });

        return countryesModelList;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
