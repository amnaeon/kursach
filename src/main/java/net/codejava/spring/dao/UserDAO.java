package net.codejava.spring.dao;

import net.codejava.spring.consts.Const;
import net.codejava.spring.model.ByerModel;
import net.codejava.spring.model.Contact;
import net.codejava.spring.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 11.04.17.
 */
public class UserDAO {
    private JdbcTemplate jdbcTemplate;

    public UserDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<User> userList(int type) {
        String sql;
        if (type == Const.User.GET_ALL_USER_TYPE)
            sql = "SELECT * FROM users";
        else sql = "SELECT * FROM users WHERE type = \"" + Const.User.USER+"\"";
        List<User> userList = jdbcTemplate.query(sql, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                User user = new User();
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setType(rs.getString("type"));
                user.setId(rs.getInt("id"));
                return user;
            }

        });

        return userList;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

}
