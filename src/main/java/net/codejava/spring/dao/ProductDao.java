package net.codejava.spring.dao;

import net.codejava.spring.model.ProductModel;
import net.codejava.spring.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 11.04.17.
 */
public class ProductDao {
    private JdbcTemplate jdbcTemplate;

    public ProductDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public List<ProductModel> productList() {
        String sql = "SELECT * FROM product";
        List<ProductModel> productList = jdbcTemplate.query(sql, new RowMapper<ProductModel>() {

            @Override
            public ProductModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                ProductModel product = new ProductModel();
                product.setSerialNumber(rs.getString("serialNumber"));
                product.setName(rs.getString("name"));
                product.setModel(rs.getString("model"));
                product.setReleaseDate(rs.getDate("releaseDate"));
                product.setAvalibilityCode(rs.getInt("availabilityCode"));
                product.setCategoryCode(rs.getInt(6));
                product.setManufacturerId(rs.getInt("manufacturerId"));
                product.setPrice(String.valueOf(rs.getFloat("price")));
                return product;
            }

        });

        return productList;
    }

    public List<ProductModel> productList1() {
        String sql = "SELECT * FROM product WHERE `availabilityCode`     = 2";
        List<ProductModel> productList = jdbcTemplate.query(sql, new RowMapper<ProductModel>() {

            @Override
            public ProductModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                ProductModel product = new ProductModel();
                product.setSerialNumber(rs.getString("serialNumber"));
                product.setName(rs.getString("name"));
                product.setModel(rs.getString("model"));
                product.setReleaseDate(rs.getDate("releaseDate"));
                product.setAvalibilityCode(rs.getInt("availabilityCode"));
                product.setCategoryCode(rs.getInt(6));
                product.setManufacturerId(rs.getInt("manufacturerId"));
                product.setPrice(String.valueOf(rs.getFloat("price")));
                return product;
            }

        });

        return productList;
    }
}
