package net.codejava.spring.dao;

import net.codejava.spring.model.LocalityTypeModel;
import net.codejava.spring.model.ManufacturerModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class ManufacturerDAO {
    private JdbcTemplate jdbcTemplate;

    public ManufacturerDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<ManufacturerModel> manufacturerModelList() {
        String sql = "SELECT * FROM manufacturers";
        List<ManufacturerModel> manufacturerModelList = jdbcTemplate.query(sql, new RowMapper<ManufacturerModel>() {

            @Override
            public ManufacturerModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                ManufacturerModel manufacturerModel = new ManufacturerModel();
                manufacturerModel.setAddress(rs.getInt("addressId"));
                manufacturerModel.setContactPerson(rs.getString("contactPerson"));
                manufacturerModel.setManufacturerId(rs.getInt("manufacturerId"));
                manufacturerModel.setManufacturerName(rs.getString("manufacturerName"));
                manufacturerModel.setTelephone(rs.getString("telephone"));
                return manufacturerModel;
            }

        });
        return manufacturerModelList;
    }
}
