package net.codejava.spring.dao;

import net.codejava.spring.model.AddressModel;
import net.codejava.spring.model.ByerModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AddressDao implements IOnBaseDAO{
    private JdbcTemplate jdbcTemplate;

    public AddressDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<AddressModel> addressList() {
        String sql = "SELECT * FROM address";
        List<AddressModel> addressModelList = jdbcTemplate.query(sql, new RowMapper<AddressModel>() {

            @Override
            public AddressModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                AddressModel addressModel = new AddressModel();
                addressModel.setAddressId(rs.getInt("addressId"));
                addressModel.setPostcode(rs.getString("postcode"));
                addressModel.setStreetId(rs.getInt("streetId"));
                addressModel.setBuildingAddress(rs.getString("buildingAddress"));
                return addressModel;
            }

        });

        return addressModelList;
    }


    @Override
    public IOnBaseDAO getDAO() {
        return this;
    }
}
