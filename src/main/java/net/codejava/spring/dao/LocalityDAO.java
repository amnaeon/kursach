package net.codejava.spring.dao;

import net.codejava.spring.model.LegalSuppliersModel;
import net.codejava.spring.model.LocalityModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class LocalityDAO {
    private JdbcTemplate jdbcTemplate;

    public LocalityDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<LocalityModel> localityModels() {
        String sql = "SELECT * FROM locality";
        List<LocalityModel> localityList = jdbcTemplate.query(sql, new RowMapper<LocalityModel>() {

            @Override
            public LocalityModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                LocalityModel locality = new LocalityModel();
                locality.setLocalityId(rs.getInt("localityId"));
                locality.setLocalityTypeId(rs.getInt("localityTypeId"));
                locality.setName(rs.getString("name"));
                locality.setTelephoneCode(rs.getString("telephoneCode"));
                locality.setRegionId(rs.getInt("regionId"));
                return locality;
            }

        });

        return localityList;
    }
}
