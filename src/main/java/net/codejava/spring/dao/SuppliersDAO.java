package net.codejava.spring.dao;

import net.codejava.spring.model.StreetsModel;
import net.codejava.spring.model.SuppliersModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class SuppliersDAO {
    private JdbcTemplate jdbcTemplate;

    public SuppliersDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<SuppliersModel> suppliersModelList() {
        String sql = "SELECT * FROM suppliers";
        List<SuppliersModel> suppliersModelList = jdbcTemplate.query(sql, new RowMapper<SuppliersModel>() {

            @Override
            public SuppliersModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                SuppliersModel suppliersModel = new SuppliersModel();
                suppliersModel.setSuppliersId(rs.getInt("suppliersId"));
                suppliersModel.setAddressId(rs.getInt("addressId"));
                suppliersModel.setPhone(rs.getString("phone"));
                suppliersModel.setTelephoneTypeId(rs.getInt("telephoneTypeId"));
                return suppliersModel;
            }

        });
//        suppliersModelList.add(new SuppliersModel(1,2,3,"dsdsd","dsd"));
        return suppliersModelList;
    }
}
