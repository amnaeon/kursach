package net.codejava.spring.dao;

import net.codejava.spring.model.RegionModel;
import net.codejava.spring.model.SalesContractsModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class SalesContractDAO {
    private JdbcTemplate jdbcTemplate;

    public SalesContractDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<SalesContractsModel> contractsModelList() {
        String sql = "SELECT * FROM salesContracts";
        List<SalesContractsModel> salesContractsModelList = jdbcTemplate.query(sql, new RowMapper<SalesContractsModel>() {

            @Override
            public SalesContractsModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                SalesContractsModel contractsModel = new SalesContractsModel();
                contractsModel.setSalesId(rs.getInt("salesId"));
                contractsModel.setByerId(rs.getInt("byerId"));
                contractsModel.setDate(rs.getString("date"));
                return contractsModel;
            }

        });
        return salesContractsModelList;
    }
}
