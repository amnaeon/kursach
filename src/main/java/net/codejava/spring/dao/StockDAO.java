package net.codejava.spring.dao;

import net.codejava.spring.model.SoldGoodsModel;
import net.codejava.spring.model.StockModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stus on 15.04.17.\
 */
public class StockDAO {
    private JdbcTemplate jdbcTemplate;

    public StockDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<StockModel> stockModels() {
//        String sql = "SELECT * FROM q";
//        List<StockModel> soldGoodsModels = jdbcTemplate.query(sql, new RowMapper<StockModel>() {
//
//            @Override
//            public StockModel mapRow(ResultSet rs, int rowNum) throws SQLException {
//                StockModel stockModel = new StockModel();
//                stockModel.setAvailabilityCode(rs.getInt(0));
//                stockModel.setAvailabilityStatus(rs.getString(1));
//                return stockModel;
//            }
//
//        });

        List<StockModel> soldGoodsModels = new ArrayList<>();
        soldGoodsModels.add(new StockModel(1,"Ожидается"));
        soldGoodsModels.add(new StockModel(2,"Есть"));
        soldGoodsModels.add(new StockModel(3,"Нету"));
        return soldGoodsModels;
    }
}
