package net.codejava.spring.dao;

import net.codejava.spring.model.RegionModel;
import net.codejava.spring.model.StreetTypeModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class RegionDAO {
    private JdbcTemplate jdbcTemplate;

    public RegionDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<RegionModel> regionModels() {
        String sql = "SELECT * FROM region";
        List<RegionModel> regionModels = jdbcTemplate.query(sql, new RowMapper<RegionModel>() {

            @Override
            public RegionModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                RegionModel regionModel = new RegionModel();
                regionModel.setRegionId(rs.getInt("regionId"));
                regionModel.setName(rs.getString("name"));
                regionModel.setCountryId(rs.getInt("countryId"));
                return regionModel;
            }

        });

        return regionModels;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
