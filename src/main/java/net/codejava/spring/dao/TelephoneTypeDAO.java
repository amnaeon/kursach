package net.codejava.spring.dao;

import net.codejava.spring.model.SupplyContractModel;
import net.codejava.spring.model.TelephoneTypeModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class TelephoneTypeDAO {
    private JdbcTemplate jdbcTemplate;

    public TelephoneTypeDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<TelephoneTypeModel> telephoneTypeModelList() {
        String sql = "SELECT * FROM telephoneType";
        List<TelephoneTypeModel> telephoneTypeModels = jdbcTemplate.query(sql, new RowMapper<TelephoneTypeModel>() {

            @Override
            public TelephoneTypeModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                TelephoneTypeModel suppliersModel = new TelephoneTypeModel();
                suppliersModel.setName(rs.getString("name"));
                suppliersModel.setTelephoneTypeId(rs.getInt("tylephoneTypeId"));
                return suppliersModel;
            }

        });

        return telephoneTypeModels;
    }
}
