package net.codejava.spring.dao;

import net.codejava.spring.model.RegionModel;
import net.codejava.spring.model.StreetTypeModel;
import net.codejava.spring.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by stus on 11.04.17.
 */
public class StreetsTypeDAO {
    private JdbcTemplate jdbcTemplate;

    public StreetsTypeDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<StreetTypeModel> streetTypeList() {
        String sql = "SELECT * FROM streetType";
        List<StreetTypeModel> streetTypeModelList = jdbcTemplate.query(sql, new RowMapper<StreetTypeModel>() {

            @Override
            public StreetTypeModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                StreetTypeModel streetTypeModel = new StreetTypeModel();
                streetTypeModel.setStreetTypeId(rs.getInt("streetTypeId"));
                streetTypeModel.setName(rs.getString("name"));
                return streetTypeModel;
            }

        });

        return streetTypeModelList;
    }


}
