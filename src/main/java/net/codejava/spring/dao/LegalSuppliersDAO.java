package net.codejava.spring.dao;

import net.codejava.spring.model.LegalByerModel;
import net.codejava.spring.model.LegalSuppliersModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class LegalSuppliersDAO {
    private JdbcTemplate jdbcTemplate;

    public LegalSuppliersDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<LegalSuppliersModel> legalSuppliersModels() {
        String sql = "SELECT * FROM legalSuppliers";
        List<LegalSuppliersModel> supliersList = jdbcTemplate.query(sql, new RowMapper<LegalSuppliersModel>() {

            @Override
            public LegalSuppliersModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                LegalSuppliersModel byer = new LegalSuppliersModel();
                byer.setSuppliersId(rs.getInt("suppliersId"));
                byer.setCertificateNumber(rs.getString("certificateNumber"));
                byer.setName(rs.getString("name"));
                byer.setTaxNumer(rs.getString("taxNumber"));
                return byer;
            }

        });

        return supliersList;
    }
}
