package net.codejava.spring.dao;

import net.codejava.spring.model.DiscountsModel;
import net.codejava.spring.model.GoodsCategoryModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class GoodsCategoryDAO {
    private JdbcTemplate jdbcTemplate;

    public GoodsCategoryDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<GoodsCategoryModel>  goodsCategoryModels() {
        String sql = "SELECT * FROM goodsCategory";
        List<GoodsCategoryModel> goodsCategoryList = jdbcTemplate.query(sql, new RowMapper<GoodsCategoryModel>() {

            @Override
            public GoodsCategoryModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                GoodsCategoryModel category = new GoodsCategoryModel();
                category.setCategoryId(rs.getInt("categoryId"));
                category.setCategoryName(rs.getString("categoryName"));
                return category;
            }

        });

        return goodsCategoryList;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}

