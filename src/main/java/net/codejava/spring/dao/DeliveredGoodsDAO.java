package net.codejava.spring.dao;

import net.codejava.spring.model.DeliveredGoodsModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class DeliveredGoodsDAO {
    private JdbcTemplate jdbcTemplate;

    public DeliveredGoodsDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<DeliveredGoodsModel> deliveredGoodsModelList() {
        String sql = "SELECT * FROM deliveredGoods";
        List<DeliveredGoodsModel> deliveredGoodsModels = jdbcTemplate.query(sql, new RowMapper<DeliveredGoodsModel>() {

            @Override
            public DeliveredGoodsModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                DeliveredGoodsModel goods = new DeliveredGoodsModel();
                goods.setSupplyContractId(rs.getString("supplyContractId"));
                goods.setName(rs.getString("name"));
                goods.setModel(rs.getString("model"));
                goods.setCategoryCode(rs.getInt("categoryId"));
                goods.setManufacturerId(rs.getInt("manufacturerId"));
                goods.setPrice(String.valueOf(rs.getInt("price")));
                goods.setCount(String.valueOf(rs.getInt("count")));
                return goods;
            }

        });

        return deliveredGoodsModels ;
    }
}
