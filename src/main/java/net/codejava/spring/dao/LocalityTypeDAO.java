package net.codejava.spring.dao;

import net.codejava.spring.model.LocalityModel;
import net.codejava.spring.model.LocalityTypeModel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by stus on 15.04.17.
 */
public class LocalityTypeDAO {
    private JdbcTemplate jdbcTemplate;

    public LocalityTypeDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<LocalityTypeModel> localityTypeModels() {
        String sql = "SELECT * FROM localityType";
        List<LocalityTypeModel> localityTypeList = jdbcTemplate.query(sql, new RowMapper<LocalityTypeModel>() {

            @Override
            public LocalityTypeModel mapRow(ResultSet rs, int rowNum) throws SQLException {
                LocalityTypeModel locality = new LocalityTypeModel();
                locality.setLocalityTypeId(rs.getInt("localityTypeId"));
                locality.setLocalityName(rs.getString("localityName"));
                return locality;
            }

        });

        return localityTypeList;
    }
}
